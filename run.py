from uni import create_app

app = create_app(env="development")
app.run('0.0.0.0')
