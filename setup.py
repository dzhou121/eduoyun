from setuptools import setup, find_packages

requires = [
    'flask',
    'flask-mail',
    'flask-sqlalchemy',
    'flask-babel',
    'flask-login',
    'flask-cache',
    'flask-mail',
    'flask-wtf',
    'flask-debugtoolbar',
    'celery-with-redis',
    'httplib2',
    'MySQL-python',
    'python-memcached',
    'ipaddr',
    'pinyin',
    'redis',
]

setup(name='uni',
      version='0.1',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires)
