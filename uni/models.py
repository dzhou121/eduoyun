import datetime

from sqlalchemy import Column, Integer, Numeric, String
from sqlalchemy import DateTime, Boolean, ForeignKey, Unicode

from uni import db


class MyBase(object):
    created_at = Column(DateTime, default=datetime.datetime.now)
    updated_at = Column(DateTime, onupdate=datetime.datetime.now)
    deleted_at = Column(DateTime)
    deleted = Column(Boolean, default=False)

    def update(self, values):
        """Make the model object behave like a dict"""
        for k, v in values.iteritems():
            setattr(self, k, v)

    def delete(self):
        self.deleted = True
        self.deleted_at = datetime.datetime.now()
        if hasattr(self, 'status'):
            self.status = 'deleted'


class User(db.Model, MyBase):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(255))
    password = Column(String(255))
    group = Column(String(255))


class UserContact(db.Model, MyBase):
    __tablename__ = 'user_contacts'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey(User.id))
    name = Column(Unicode(255))
    email = Column(String(255))
    company = Column(Unicode(255))
    telephone = Column(String(255))
    telephone_cc = Column(String(255))
    address = Column(Unicode(255))
    city = Column(Unicode(255))
    province = Column(Unicode(255))
    country = Column(Unicode(255))
    iso2 = Column(String(255))
    postcode = Column(String(255))
    default = Column(Boolean)
    resellbiz_id = Column(Integer)


class HostingProduct(db.Model, MyBase):
    __tablename__ = "hosting_products"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    num_websites = Column(Integer)
    num_mysqldbs = Column(Integer)
    bandwidth = Column(Integer)
    disk = Column(Integer)
    order = Column(Integer)


class UserHosting(db.Model, MyBase):
    __tablename__ = 'user_hostings'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey(User.id))
    product_id = Column(Integer, ForeignKey(HostingProduct.id))
    expires_at = Column(DateTime)
    status = Column(String(255))
    osuser_id = Column(Integer)


class HostingPrice(db.Model, MyBase):
    __tablename__ = 'hosting_prices'
    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, ForeignKey(HostingProduct.id))
    term = Column(String(255))
    price = Column(Numeric(10, 2))


class Voucher(db.Model, MyBase):
    __tablename__ = 'vouchers'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    uses = Column(Integer)
    max_uses = Column(Integer)
    description = Column(String(255))


class Invoice(db.Model, MyBase):
    __tablename__ = 'invoices'
    id = Column(Integer, primary_key=True)
    uuid = Column(String(255))
    paid = Column(Boolean, default=False)
    paid_at = Column(DateTime)
    user_id = Column(Integer, ForeignKey(User.id))
    voucher_id = Column(Integer, ForeignKey(Voucher.id))


class InvoiceItem(db.Model, MyBase):
    __tablename__ = 'invoice_items'
    id = Column(Integer, primary_key=True)
    term = Column(String(255))
    amount = Column(Numeric(10, 2))
    discount = Column(Numeric(10, 2))
    invoice_id = Column(Integer, ForeignKey(Invoice.id))
    item_id = Column(Integer)
    product_id = Column(Integer)
    category = Column(String(255))
    type = Column(String(255))
    refunded = Column(Boolean, default=False)
    refunded_at = Column(DateTime)


class DomainTld(db.Model, MyBase):
    __tablename__ = 'domain_tlds'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))


class DomainPrice(db.Model, MyBase):
    __tablename__ = 'domain_prices'
    id = Column(Integer, primary_key=True)
    tld_id = Column(Integer, ForeignKey(DomainTld.id))
    term = Column(String(255))
    price = Column(Numeric(10, 2))


class UserDomain(db.Model, MyBase):
    __tablename__ = 'user_domains'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey(User.id))
    name = Column(String(255))
    tld_id = Column(Integer, ForeignKey(DomainTld.id))
    status = Column(String(255))
    expires_at = Column(DateTime)
    reg_contact = Column(Integer, ForeignKey(UserContact.id))
    admin_contact = Column(Integer, ForeignKey(UserContact.id))
    tech_contact = Column(Integer, ForeignKey(UserContact.id))
    billing_contact = Column(Integer, ForeignKey(UserContact.id))
    ns = Column(String(255))
    auth_code = Column(String(255))
    resellbiz_order_id = Column(String(255))


class UserDomainDNS(db.Model, MyBase):
    __tablename__ = 'user_domain_dns'
    id = Column(Integer, primary_key=True)
    domain_id = Column(Integer, ForeignKey(UserDomain.id))
    status = Column(String(255))
    hostname = Column(String(255))
    type = Column(String(255))
    content = Column(String(255))
    priority = Column(String(255))
    ttl = Column(Integer)


class UserDomainGlue(db.Model, MyBase):
    __tablename__ = 'user_domain_glue'
    id = Column(Integer, primary_key=True)
    domain_id = Column(Integer, ForeignKey(UserDomain.id))
    status = Column(String(255))
    hostname = Column(String(255))
    ip = Column(String(255))
