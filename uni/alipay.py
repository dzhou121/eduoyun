# coding=utf-8

import hashlib
import urllib2
import urllib


class Alipay(object):
    def __init__(self):
        self.params = {}
        self.pay_gate_way = 'https://www.alipay.com/cooperate/gateway.do'
        self.security_code = 'z'
        self.params['partner'] = '1'
        self.params['seller_email'] = 'aneckowen@163.com'
        self.params['payment_type'] = '04'
        self.params['_input_charset'] = 'utf-8'
        self.params['service'] = 'trade_create_by_buyer'
        self.params['notify_url'] = 'https://www.eduoyun.com/alipay'
        self.params['logistics_type'] = 'EXPRESS'
        self.params['logistics_fee'] = '0'
        self.params['logistics_payment'] = 'SELLER_PAY'
        self.params['receive_name'] = '不需要物流'
        self.params['receive_address'] = '不需要物流'
        self.params['receive_zip'] = '000000'

    def conv_uni(self, param, input_charset):
        data = param
        if type(data) is unicode:
            data = data.encode(input_charset)
        return data

    def sign(self, params, sign_type='MD5', input_charset='utf-8'):
        param_keys = []
        sign = ''

        param_keys = params.keys()
        param_keys.sort()
        unsigned_data = ''
        for key in param_keys:
            data = self.conv_uni(params[key], input_charset)
            key = self.conv_uni(key, input_charset)
            unsigned_data += key + '=' + data
            if key != param_keys[-1]:
                unsigned_data += '&'
        unsigned_data += self.security_code
        if sign_type == 'MD5':
            sign = hashlib.md5(unsigned_data).hexdigest()
        else:
            sign = ''

        return param_keys, sign

    def _create_url(self, params, sign_type='MD5'):
        ''' Make sure using unicode or utf-8 as params
        '''
        param_keys, sign = self.sign(params,
                                     input_charset=params['_input_charset'])

        request_data = self.pay_gate_way + '?'
        for key in param_keys:
            data = params[key]
            if type(data) is unicode:
                data = data.encode(params['_input_charset'])
            request_data += key + '=' + urllib.quote(data, '')
            request_data += '&'
        request_data += 'sign=' + sign + '&sign_type=' + sign_type
        # 返回结果
        return request_data

    def create_url(self,
                   out_trade_no,
                   total_fee,
                   body=None):
        self.params['subject'] = '易多云订单'
        if body:
            self.params['body'] = body
        self.params['out_trade_no'] = str(out_trade_no)
        self.params['price'] = str(total_fee)
        self.params['quantity'] = '1'
        # 返回结果
        return self._create_url(self.params)

    def confirm_goods(self, trade_no):
        params = {}
        params['service'] = 'send_goods_confirm_by_platform'
        params['partner'] = self.params['partner']
        params['_input_charset'] = self.params['_input_charset']
        params['trade_no'] = trade_no
        params['logistics_name'] = '自动发货'
        params['transport_type'] = 'EXPRESS'
        url = self._create_url(params)
        req = urllib2.Request(url)
        urllib2.urlopen(req, {})

    def validate(self, request):
        if request.method == 'POST':
            data_dict = request.form
            sign_type = data_dict.get('sign_type', '').lower()
            if sign_type == 'md5':
                sign = data_dict.get('sign', '')
                params = {}
                for k, v in data_dict.items():
                    if k not in ('sign', 'sign_type'):
                        params[k] = v

                # 签名比较防篡改
                keys, our_sign = self.sign(params)
                if sign == our_sign:
                    # 验证notify_id
                    notify_id = params['notify_id']
                    notify_verify_url = self.create_notify_verify_url(
                        notify_id)

                    req = urllib2.Request(notify_verify_url)
                    fd = urllib2.urlopen(req, {})
                    rsp = fd.read()
                    if rsp == 'true':
                        return True, params
        return False, {}

    def create_notify_verify_url(self, notify_id):
        request_data = self.pay_gate_way + '?service=notify_verify'
        request_data += '&partner=' + self.params['partner']
        request_data += '&notify_id=' + notify_id
        return request_data
