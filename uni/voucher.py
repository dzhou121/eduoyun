from uni import db
from uni.models import Voucher

try:
    import simplejson as json
except ImportError:
    import json


class API(object):

    def get_by_name(self, name):
        voucher = db.session.query(Voucher).\
                filter_by(name=name).\
                filter_by(deleted=False).\
                filter(Voucher.uses<Voucher.max_uses).\
                first()
        if not voucher:
            return {}
        return self._basic(voucher)

    def _basic(self, voucher):
        return {"name": voucher.name,
                "description": json.loads(voucher.description),
                "id": voucher.id, }

    def increment_uses(self, name):
        voucher = db.session.query(Voucher).\
                filter_by(name=name).\
                filter_by(deleted=False).\
                filter(Voucher.uses<Voucher.max_uses).\
                first()
        voucher.uses += 1
        db.session.flush()
