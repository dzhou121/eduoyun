import ipaddr
import datetime
import pinyin
from decimal import Decimal
from flask import current_app as app

from uni import db
from uni.models import DomainTld, DomainPrice
from uni.models import User, UserDomain, UserContact, UserDomainDNS
from uni.models import UserDomainGlue, InvoiceItem, Invoice
from uni.domain import resellbiz
from uni.country import COUNTRY

try:
    import simplejson as json
except ImportError:
    import json


class API(object):

    def __init__(self):
        self.resellbiz = resellbiz.API()

    def create(self, user_id, domain_name, tld_id,
               reg_contact, admin_contact, tech_contact, billing_contact,
               auth_code=None):
        domain = UserDomain(tld_id=tld_id,
                            name=domain_name,
                            user_id=user_id,
                            reg_contact=reg_contact,
                            admin_contact=admin_contact,
                            tech_contact=tech_contact,
                            billing_contact=billing_contact,
                            auth_code=auth_code,
                            status='pending_payment')
        db.session.add(domain)
        db.session.flush()
        tld = db.session.query(DomainTld).\
            filter_by(id=tld_id).\
            first()
        return self._domain_basic(domain, tld)

    def create_contact(self, user_id, name, email, company, telephone,
                       address, city, province, country, postcode,
                       create_resellbiz=True):
        for country_name in COUNTRY:
            if country_name['cn'] == country:
                iso2 = country_name['iso2'].upper()
                telephone_cc = country_name['phone_cc']
                break

        resellbiz_id = None
        if create_resellbiz:
            resp = self.create_resellbiz_contact(
                name, email, company, telephone, address,
                city, province, country, iso2, telephone_cc, postcode)
            if resp['status'] == 'failed':
                app.logger.error(json.dumps(resp))
                return resp
            else:
                resellbiz_id = resp['resellbiz_id']

        contact = UserContact(user_id=user_id, name=name, email=email,
                              company=company, telephone=telephone,
                              address=address, city=city, province=province,
                              country=country, iso2=iso2,
                              telephone_cc=telephone_cc, postcode=postcode,
                              resellbiz_id=resellbiz_id)
        db.session.add(contact)
        db.session.flush()
        return {"id": contact.id,
                "status": "success", }

    def create_resellbiz_contact(self, name, email, company, telephone,
                                 address, city, province, country, iso2,
                                 telephone_cc, postcode):
        contact_dict = {
            'name': pinyin.get_pinyin(unicode(name)),
            'email': email,
            'company': pinyin.get_pinyin(unicode(company)),
            'address': pinyin.get_pinyin(unicode(address)),
            'telephone': telephone,
            'telephone_cc': telephone_cc,
            'city': pinyin.get_pinyin(unicode(city)),
            'country': iso2,
            'province': pinyin.get_pinyin(unicode(province)),
            'postcode': postcode,
        }
        resp = self.resellbiz.contact.create(contact_dict)
        if not isinstance(resp, dict):
            return {'status': 'success',
                    'resellbiz_id': resp}
        else:
            resp.update({'status': 'failed'})
            return resp

    def get_contact_by_id(self, id):
        contact = db.session.query(UserContact).\
            filter_by(id=id).\
            first()
        return self._contact_basic(contact)

    def get_contacts_by_username(self, username):
        contacts = db.session.query(UserContact).\
            join(User).\
            filter(UserContact.deleted == 0).\
            filter(User.username == username).\
            filter(UserContact.resellbiz_id != None).\
            all()

        return [self._contact_basic(contact) for contact in contacts]

    def _contact_basic(self, contact):
        return {'id': contact.id,
                'name': contact.name,
                'company': contact.company,
                'email': contact.email,
                'telephone': contact.telephone,
                'address': contact.address,
                'city': contact.city,
                'province': contact.province,
                'country': contact.country,
                'iso2': contact.iso2,
                'telephone_cc': contact.telephone_cc,
                'postcode': contact.postcode,
                'resellbiz_id': contact.resellbiz_id,
                'default': contact.default,}

    def _update_contact(self, contact_id, values):
        contact = db.session.query(UserContact).\
            filter_by(id=contact_id).\
            first()
        contact.update(values)
        db.session.flush()

    def register(self, domain_id, term):
        domain = self.get_domain_by_id(domain_id)
        self._update_domain(domain_id, {'status': 'pending_register'})

        contact = self.get_contact_by_id(domain['reg_contact'])
        if contact['resellbiz_id'] is None:
            resp = self.create_resellbiz_contact(
                contact['name'], contact['email'], contact['company'],
                contact['telephone'], contact['address'], contact['city'],
                contact['province'], contact['country'], contact['iso2'],
                contact['telephone_cc'], contact['postcode'])
            if resp['status'] == 'failed':
                return
            else:
                resellbiz_contact_id = resp['resellbiz_id']
                self._update_contact(contact['id'], {'resellbiz_id':
                                                     resellbiz_contact_id})
        else:
            resellbiz_contact_id = contact['resellbiz_id']

        resp = self.resellbiz.domain.register(
            '%s.%s' % (domain['name'], domain['tld']),
            term[:-1],
            resellbiz_contact_id,
            resellbiz_contact_id,
            resellbiz_contact_id,
            resellbiz_contact_id,)
        print '---------------- now register --------------------'
        print resp

        if resp['status'] != 'Success':
            return

        order_id = resp['entityid']
        details = self.resellbiz.domain.get_details(order_id, 'OrderDetails')
        expires = datetime.datetime.fromtimestamp(int(details['endtime']))
        self._update_domain(domain_id, {'expires_at': expires,
                                        'auth_code': details['domsecret'],
                                        'status': 'active',
                                        'ns': json.dumps(resellbiz.defaultns),
                                        'resellbiz_order_id': order_id, })

        self.resellbiz.dns.active(order_id)

    def renew(self, domain_id, term):
        domain = self.get_domain_by_id(domain_id)
        expires = domain['expires_at']
        expires = expires.strftime("%s")
        resp = self.resellbiz.domain.renew(domain['resellbiz_order_id'],
                                           term[:-1],
                                           expires)
        print '----------------------resp---------------------------'
        print resp
        
        if resp['status'] != 'Success':
            return
        details = self.resellbiz.domain.\
                get_details(domain['resellbiz_order_id'],
                            'OrderDetails')
        expires = datetime.datetime.fromtimestamp(int(details['endtime']))
        self._update_domain(domain_id, {'expires_at': expires,
                                        'status': 'active', })

    def transfer_in(self, domain_id, term):
        domain = self.get_domain_by_id(domain_id)
        self._update_domain(domain_id, {'status': 'pending_transfer_in'})

        contact = self.get_contact_by_id(domain['reg_contact'])
        if contact['resellbiz_id'] is None:
            resp = self.create_resellbiz_contact(
                contact['name'], contact['email'], contact['company'],
                contact['telephone'], contact['address'], contact['city'],
                contact['province'], contact['country'], contact['iso2'],
                contact['telephone_cc'], contact['postcode'])
            if resp['status'] == 'failed':
                return
            else:
                resellbiz_contact_id = resp['resellbiz_id']
                self._update_contact(contact['id'], {'resellbiz_id':
                                                     resellbiz_contact_id})
        else:
            resellbiz_contact_id = contact['resellbiz_id']

        resp = self.resellbiz.domain.transfer_in(
            '%s.%s' % (domain['name'], domain['tld']),
            domain['auth_code'],
            term[:-1],
            resellbiz_contact_id,
            resellbiz_contact_id,
            resellbiz_contact_id,
            resellbiz_contact_id,)
        print '---------------- now transfer --------------------'
        print resp

        if resp['status'] != 'Success':
            return

        order_id = resp['entityid']
        details = self.resellbiz.domain.get_details(order_id, 'OrderDetails')
        expires = datetime.datetime.fromtimestamp(int(details['endtime']))
        self._update_domain(domain_id, {'expires_at': expires,
                                        'auth_code': details['domsecret'],
                                        'status': 'active',
                                        'ns': json.dumps(resellbiz.defaultns),
                                        'resellbiz_order_id': order_id, })

        self.resellbiz.dns.active(order_id)

    def _update_domain(self, domain_id, values):
        domain = db.session.query(UserDomain).\
            filter_by(id=domain_id).\
            first()

        if domain is not None:
            domain.update(values)
            db.session.commit()

    def get_tlds(self):
        tlds = db.session.query(DomainTld).\
                filter_by(deleted=False).\
                all()
        return [{'id': tld.id, 'name': tld.name, } for tld in tlds]

    def get_tld_by_name(self, name):
        tld = db.session.query(DomainTld).\
                filter_by(deleted=False).\
                filter_by(name=name).\
                first()

        return {'id': tld.id, 'name': tld.name}

    def get_tld_by_id(self, id):
        tld = db.session.query(DomainTld).\
                filter_by(deleted=False).\
                filter_by(id=id).\
                first()

        return {'id': tld.id, 'name': tld.name}

    def get_prices(self, tld_id, voucher):
        prices = db.session.query(DomainPrice).\
                filter(DomainPrice.tld_id==tld_id).\
                filter(DomainPrice.deleted==False).\
                all()
        return [self._price_basic(price, voucher) for price in prices]

    def _price_basic(self, price, voucher):
        discount = Decimal('0.00')
        for voucher_item in voucher:
            if voucher_item['price_id'] == price.id and \
               voucher_item['category'] == 'domain' and \
               voucher_item['numbers'] > 0:
                discount = Decimal(str(voucher_item['discount']))
                voucher_item['numbers'] -= 1
        term = price.term
        price = price.price
        if price - discount < 0:
            discount = price
            price = Decimal('0.00')
        else:
            price = price - discount
        return {"term": term,
                "price": price,
                "discount": discount, }

    def get_price_by_term(self, tld, term, voucher):
        price = db.session.query(DomainPrice).\
                join(DomainTld).\
                filter(DomainPrice.deleted==False).\
                filter(DomainTld.deleted==False).\
                filter(DomainPrice.term==term).\
                filter(DomainTld.name==tld).\
                first()
        if not price:
            return {}
        else:
            return {"price": price.price,
                    "discount": Decimal(0.00), }

    def check_availability(self, domain_name, tld_id):
        tld = db.session.query(DomainTld).\
                filter_by(deleted=False).\
                filter_by(id=tld_id).\
                first()
        if not tld:
            return 'notfound'

        availability = self.resellbiz.domain.check_availability(domain_name,
                                                                tld.name)
        availability = availability.get('%s.%s' % (domain_name, tld.name), {})
        if 'status' in availability:
            return availability['status']
        else:
            return 'unknown'

    def get_domains_by_username(self, username):
        result = db.session.query(UserDomain, DomainTld).\
                join(DomainTld).\
                join(User).\
                filter(User.username==username).\
                filter(User.deleted==False).\
                filter(DomainTld.deleted==False).\
                filter(UserDomain.deleted==False).\
                filter(UserDomain.status=='active').\
                all()

        return [self._domain_basic(domain, tld)
                for domain, tld in result]

    def get_domain_by_id_and_username(self, id, username):
        result = db.session.query(UserDomain, DomainTld).\
                join(DomainTld).\
                join(User).\
                filter(User.deleted==False).\
                filter(DomainTld.deleted==False).\
                filter(UserDomain.deleted==False).\
                filter(User.username==username).\
                filter(UserDomain.id==id).\
                first()

        if result is not None:
            domain, tld = result
            return self._domain_basic(domain, tld)

    def get_domain_by_id(self, id):
        result = db.session.query(UserDomain, DomainTld).\
            join(DomainTld).\
            filter(DomainTld.deleted == 0).\
            filter(UserDomain.deleted == 0).\
            filter(UserDomain.id == id).\
            first()

        if result is not None:
            domain, tld = result
            return self._domain_basic(domain, tld)

    def _domain_basic(self, domain, tld):
        return {"id": domain.id,
                "tld": tld.name,
                "tld_id": tld.id,
                "name": domain.name,
                "expires_at": domain.expires_at,
                "auth_code": domain.auth_code,
                "ns": domain.ns,
                "reg_contact": domain.reg_contact,
                "admin_contact": domain.admin_contact,
                "tech_contact": domain.tech_contact,
                "resellbiz_order_id": domain.resellbiz_order_id,
                "billing_contact": domain.billing_contact,
                "status": domain.status,}

    def get_dns_by_domain_id(self, domain_id):
        records = db.session.query(UserDomainDNS).\
                filter_by(domain_id=domain_id).\
                filter_by(deleted=False).\
                order_by(UserDomainDNS.type).\
                order_by(UserDomainDNS.hostname).\
                order_by(UserDomainDNS.priority).\
                all()
        return [self._dns_basic(record) for record in records]

    def get_dns_by_id_and_domain_id(self, id, domain_id):
        record = db.session.query(UserDomainDNS).\
                join(UserDomain).\
                filter(UserDomainDNS.id==id).\
                filter(UserDomainDNS.deleted==False).\
                filter(UserDomain.id==domain_id).\
                filter(UserDomain.deleted==False).\
                first()
        if record:
            return self._dns_basic(record)

    def _dns_basic(self, dns):
        return {"id": dns.id,
                "status": dns.status,
                "hostname": dns.hostname,
                "type": dns.type,
                "content": dns.content,
                "priority": dns.priority,
                "ttl": dns.ttl, }

    def add_dns(self, domain_id, hostname, type, content, priority, ttl):
        domain = self.get_domain_by_id(domain_id)
        domain = "%s.%s" % (domain['name'], domain['tld'])

        if type == 'AAAA':
            try:
                content = ipaddr.IPv6Address(content).exploded
            except:
                pass

        resp = self.resellbiz.dns.add(domain, hostname, 
                                      type, content, priority, ttl)
        print '------------------ resp --------------------'
        print resp
        if resp['status'] != 'Success':
            return resp

        if type == 'AAAA':
            content = ipaddr.IPv6Address(content).exploded
        record = UserDomainDNS(domain_id=domain_id,
                               hostname=hostname,
                               type=type,
                               content=content,
                               priority=priority,
                               status='active',
                               ttl=ttl)
        db.session.add(record)
        db.session.flush()
        return self._dns_basic(record)

    def modify_dns(self, domain_id, record_id,
                   hostname, type, content, priority, ttl):
        domain = self.get_domain_by_id(domain_id)
        domain = "%s.%s" % (domain['name'], domain['tld'])

        if type == 'AAAA':
            try:
                content = ipaddr.IPv6Address(content).exploded
            except:
                pass

        record = self.get_dns_by_id_and_domain_id(record_id, domain_id)
        if hostname == record['hostname'] and type == record['type']:
            resp = self.resellbiz.dns.modify(domain, hostname, type,
                                             record['content'],content,
                                             priority, ttl)
            if resp['status'] != 'Success':
                return resp
        else:
            resp = self.resellbiz.dns.delete(domain, record['hostname'],
                                             record['type'],
                                             record['content'])
            resp = self.resellbiz.dns.add(domain, hostname, type,
                                          content, priority, ttl)
            if resp['status'] != 'Success':
                self.resellbiz.dns.add(domain, record['hostname'],
                                       record['type'],
                                       record['content'],
                                       record['priority'],
                                       record['ttl'])
                return resp
        
        record = db.session.query(UserDomainDNS).\
                filter_by(domain_id=domain_id).\
                filter_by(id=record_id).\
                first()
        value = {'hostname': hostname,
                 'type': type,
                 'content': content,
                 'priority': priority,
                 'ttl': ttl, }
        record.update(value)
        db.session.flush()
        return self._dns_basic(record)

    def delete_dns(self, domain_id, record_id):
        domain = self.get_domain_by_id(domain_id)
        domain = "%s.%s" % (domain['name'], domain['tld'])
        record = self.get_dns_by_id_and_domain_id(record_id, domain_id)
        resp = self.resellbiz.dns.delete(domain, record['hostname'],
                                         record['type'],
                                         record['content'])
        if resp['status'] != 'Success':
            return resp
        
        self._delete_dns(record_id)
        return {'status': 'Success', }

    def _delete_dns(self, id):
        record = db.session.query(UserDomainDNS).\
                filter_by(id=id).\
                first()
        record.delete()

    def update_ns(self, domain_id, new_ns):
        domain = db.session.query(UserDomain).\
                filter_by(id=domain_id).\
                first()

        if len(new_ns) < 2:
            return {'status': 'error'}

        if json.dumps(new_ns) == domain.ns:
            return {'status': 'Success'}

        resp = self.resellbiz.domain.update_ns(domain.resellbiz_order_id,
                                               new_ns)
        print '-------------------------- resp ------------------------'
        print resp
        if resp['status'] != 'Success':
            return resp

        domain.update({"ns": json.dumps(new_ns)})
        db.session.flush()
        return {'status': 'Success'}

    def get_default_ns(self):
        return self.resellbiz.domain.get_default_ns()

    def get_glue(self, domain_id):
        glues = db.session.query(UserDomainGlue).\
                join(UserDomain).\
                filter(UserDomain.deleted==False).\
                filter(UserDomainGlue.deleted==False).\
                filter(UserDomain.id==domain_id).\
                all()

        return [self._glue_basic(glue) for glue in glues]

    def change_contact(self, domain_id, reg_contact, admin_contact,
                       tech_contact, billing_contact):
        domain = self.get_domain_by_id(domain_id)
        resellbiz_reg_contact = self.get_contact_by_id(reg_contact)['resellbiz_id']
        resellbiz_admin_contact = self.get_contact_by_id(admin_contact)['resellbiz_id']
        resellbiz_tech_contact = self.get_contact_by_id(tech_contact)['resellbiz_id']
        resellbiz_billing_contact = self.get_contact_by_id(billing_contact)['resellbiz_id']
        resp = self.resellbiz.contact.change(
            domain['resellbiz_order_id'], resellbiz_reg_contact, 
            resellbiz_admin_contact, resellbiz_tech_contact, 
            resellbiz_billing_contact)

        print '--------------resp---------------------'
        print resp

        if resp['status'] != 'Success':
            return resp

        self._update_domain(domain_id, 
                            { 'reg_contact': reg_contact,
                             'admin_contact': admin_contact,
                             'tech_contact': tech_contact,
                             'billing_contact': billing_contact, })
        return {'status': 'success'}

    def add_glue(self, domain_id, hostname, ip):
        domain = self.get_domain_by_id(domain_id)
        gluename = "%s.%s.%s" % (hostname, domain['name'], domain['tld'])

        resp = self.resellbiz.domain.add_glue(domain['resellbiz_order_id'],
                                               gluename,
                                               ip)
        print '------------------ resp --------------------'
        print resp
        if resp['status'] != 'Success':
            return resp

        glue = UserDomainGlue(hostname=hostname,
                              domain_id=domain_id,
                              ip=ip,
                              status='active')
        db.session.add(glue)
        db.session.flush()
        return self._glue_basic(glue)

    def modify_glue(self, domain_id, glue_id, hostname, ip):
        domain = self.get_domain_by_id(domain_id)
        new_gluename = "%s.%s.%s" % (hostname, domain['name'], domain['tld'])

        glue = self.get_glue_by_id(glue_id)
        old_gluename = "%s.%s.%s" % (glue['hostname'],
                                     domain['name'],
                                     domain['tld'])
        if new_gluename != old_gluename and ip == glue['ip']:
            resp = self.resellbiz.domain.\
                modify_glue_name(domain['resellbiz_order_id'],
                                 old_gluename,
                                 new_gluename, )
            if resp['status'] != 'Success':
                return resp
        elif new_gluename == old_gluename and ip != glue['ip']:
            resp = self.resellbiz.domain.\
                modify_glue_ip(domain['resellbiz_order_id'],
                               old_gluename,
                               glue['ip'],
                               ip)
            print '------------------ resp --------------------'
            print resp
            if resp['status'] != 'Success':
                return resp
        elif new_gluename != old_gluename and ip != glue['ip']:
            resp = self.resellbiz.domain.add_glue(domain['resellbiz_order_id'],
                                                   new_gluename,
                                                   ip)
            print '------------------ resp --------------------'
            print resp
            if resp['status'] != 'Success':
                return resp
            resp = self.resellbiz.domain.delete_glue(
                domain['resellbiz_order_id'],
                old_gluename,
                glue['ip'])
            print '------------------ resp --------------------'
            print resp
            if resp['status'] != 'Success':
                return resp
        else:
            return glue

        glue = self._update_glue(glue_id, {'hostname': hostname,
                                           'ip': ip, })
        return glue

    def _update_glue(self, id, values):
        glue = db.session.query(UserDomainGlue).\
            filter_by(id=id).\
            first()
        glue.update(values)
        db.session.flush()
        return self._glue_basic(glue)

    def get_glue_by_id(self, id):
        glue = db.session.query(UserDomainGlue).\
            filter_by(id=id).\
            first()

        if glue is not None:
            return self._glue_basic(glue)

    def delete_glue(self, domain_id, glue_id):
        domain = self.get_domain_by_id(domain_id)
        glue = db.session.query(UserDomainGlue).\
                join(UserDomain).\
                filter(UserDomain.id==domain_id).\
                filter(UserDomainGlue.id==glue_id).\
                filter(UserDomainGlue.deleted==False).\
                first()

        if glue is None:
            return

        gluename = "%s.%s.%s" % (glue.hostname, domain['name'], domain['tld'])

        resp = self.resellbiz.domain.delete_glue(domain['resellbiz_order_id'],
                                                 gluename,
                                                 glue.ip)
        print '------------------ resp --------------------'
        print resp
        if resp['status'] != 'Success':
            return resp

        glue.delete()
        db.session.flush()
        return {'status': 'Success'}

    def _glue_basic(self, glue):
        return {'id': glue.id,
                'hostname': glue.hostname,
                'ip': glue.ip,
                'status': glue.status, }

    def get_pending(self):
        result = db.session.query(UserDomain, DomainTld,
                                  User, InvoiceItem, Invoice).\
            join(User, UserDomain.user_id == User.id).\
            join(DomainTld, UserDomain.tld_id == DomainTld.id).\
            join(InvoiceItem, InvoiceItem.item_id == UserDomain.id).\
            join(Invoice, Invoice.id == InvoiceItem.invoice_id).\
            filter(Invoice.paid == 1).\
            filter(UserDomain.status.like("pending%")).\
            all()
        return [self._pending_basic(domain, tld, user, invoice_item, invoice)
                for domain, tld, user, invoice_item, invoice in result]

    def _pending_basic(self, domain, tld, user, invoice_item, invoice):
        return {
            'username': user.username,
            'user_id': user.id,
            'tld': tld.name,
            'paid': invoice.paid,
            'paid_at': invoice.paid_at,
            'name': domain.name,
            'status': domain.status,
            'type': invoice_item.type,
        }
