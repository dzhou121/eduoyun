import urllib2
import httplib2

try:
    import simplejson as json
except ImportError:
    import json

from uni import app


base_url = app.config['BASE_URL']
userid = app.config['USERID']
password = app.config['PASSWORD']
customerid = app.config['CUSTOMERID']
defaultns = app.config['DEFAULTNS']
privacy = app.config['PRIVACY']
invoiceoption = app.config['INVOICEOPTION']


class HTTPClient(object):

    def __init__(self, base_url):
        self.h = httplib2.Http()
        self.base_url = base_url

    def _request(self, url, method, params, **kwargs):
        base_url = self.base_url

        def convert(key, value):
            if isinstance(value, list):
                return '&'.join(map(lambda x: '%s=%s' % (key, x), value))
            else:
                return '%s=%s' % (key, value)
        param = [convert(key, value) for key, value in params.items()]
        url = '%s%s?%s' % (base_url, url, '&'.join(param))

        resp, body = self.h.request(url, method, **kwargs)

        if body:
            try:
                body = json.loads(body)
            except ValueError:
                body = {"badMethod": {"message": "Unknown Error"}}

        return body

    def get(self, url, params):
        return self._request(url, 'GET', params)

    def post(self, url, params, body):
        return self._request(url, 'POST', params, body=body)

    def put(self, url, params, body):
        return self._request(url, 'PUT', params, body=body)

    def delete(self, url, params):
        return self._request(url, 'DELETE', params)


class ClientManager(object):

    def __init__(self, client):
        self.client = client
        self.params = {'auth-userid': userid,
                       'auth-password': password, }

    def _get(self, url, params):
        params.update(self.params)
        return self.client.http.get(url, params)

    def _post(self, url, params, body=None):
        params.update(self.params)
        return self.client.http.post(url, params, body)

    def _put(self, url, params, body=None):
        params.update(self.params)
        return self.client.http.put(url, params, body)

    def _delete(self, url, params, body=None):
        params.update(self.params)
        return self.client.http.delete(url, params)


class Domain(ClientManager):

    def check_availability(self, domain_name, tld):
        params = {'domain-name': domain_name,
                  'tlds': tld, }
        url = '/api/domains/available.json'
        return self._get(url, params)

    def get_default_ns(self):
        return defaultns

    def register(self, domain, years,
                 reg_contact_id,
                 admin_contact_id,
                 tech_contact_id,
                 billing_contact_id,
                 customer_id=None,
                 invoice_option=None,
                 ns=defaultns, protect_privacy=None):
        url = '/api/domains/register.json'
        params = {
            'domain-name': domain,
            'years': years,
            'ns': ns or defaultns,
            'customer-id': customer_id or customerid,
            'reg-contact-id': reg_contact_id,
            'admin-contact-id': admin_contact_id,
            'tech-contact-id': tech_contact_id,
            'billing-contact-id': billing_contact_id,
            'invoice-option': invoice_option or invoiceoption,
            'protect-privacy': protect_privacy or privacy,
        }
        return self._post(url, params)

    def transfer_in(self, domain, auth_code, years,
                    reg_contact_id,
                    admin_contact_id,
                    tech_contact_id,
                    billing_contact_id,
                    customer_id=None,
                    invoice_option=None,
                    ns=defaultns, protect_privacy=None):
        url = '/api/domains/transfer.json'
        params = {
            'domain-name': domain,
            'years': years,
            'auth-code': urllib2.quote(auth_code),
            'ns': ns or defaultns,
            'customer-id': customer_id or customerid,
            'reg-contact-id': reg_contact_id,
            'admin-contact-id': admin_contact_id,
            'tech-contact-id': tech_contact_id,
            'billing-contact-id': billing_contact_id,
            'invoice-option': invoice_option or invoiceoption,
            'protect-privacy': protect_privacy or privacy,
        }
        return self._post(url, params)

    def get_order_id(self, domain):
        url = '/api/domains/orderid.json'
        params = {'domain-name': domain}
        return self._get(url, params)

    def get_details(self, order_id, options):
        url = '/api/domains/details.json'
        params = {'order-id': order_id,
                  'options': options, }
        return self._get(url, params)

    def renew(self, order_id, years, expires, invoice_option=None):
        url = '/api/domains/renew.json'
        params = {'order-id': order_id,
                  'years': years,
                  'exp-date': expires,
                  'invoice-option': invoice_option or invoiceoption, }
        return self._post(url, params)

    def update_ns(self, order_id, new_ns):
        url = '/api/domains/modify-ns.json'
        params = {'order-id': order_id,
                  'ns': new_ns, }
        return self._post(url, params)

    def add_glue(self, order_id, hostname, ip):
        url = '/api/domains/add-cns.json'
        params = {'order-id': order_id,
                  'cns': hostname,
                  'ip': ip, }
        return self._post(url, params)

    def modify_glue_name(self, order_id, old_gluename, new_gluename):
        url = '/api/domains/modify-cns-name.json'
        params = {'order-id': order_id,
                  'old-cns': old_gluename,
                  'new-cns': new_gluename, }
        return self._post(url, params)

    def modify_glue_ip(self, order_id, gluename, old_ip, new_ip):
        url = '/api/domains/modify-cns-ip.json'
        params = {'order-id': order_id,
                  'cns': gluename,
                  'old-ip': old_ip,
                  'new-ip': new_ip, }
        return self._post(url, params)

    def delete_glue(self, order_id, hostname, ip):
        url = '/api/domains/delete-cns-ip.json'
        params = {'order-id': order_id,
                  'cns': hostname,
                  'ip': ip, }
        return self._post(url, params)


class Contact(ClientManager):

    def create(self, contact, customer_id=None, type='Contact'):
        url = '/api/contacts/add.json'
        params = {
            'name': contact['name'],
            'email': contact['email'],
            'company': contact['company'],
            'address-line-1': contact['address'],
            'city': contact['city'],
            'country': contact['country'],
            'state': contact['province'],
            'zipcode': contact['postcode'],
            'phone-cc': contact['telephone_cc'],
            'phone': contact['telephone'],
            'customer-id': customer_id or customerid,
            'type': type,
        }
        return self._post(url, params)

    def change(self, order_id, reg_contact_id, admin_contact_id,
               tech_contact_id, billing_contact_id):
        url = '/api/domains/modify-contact.json'
        params = {
            'order-id': order_id,
            'reg-contact-id': reg_contact_id,
            'admin-contact-id': admin_contact_id,
            'tech-contact-id': tech_contact_id,
            'billing-contact-id': billing_contact_id,
        }
        return self._post(url, params)


class DNS(ClientManager):

    types = {
        'A': 'ipv4',
        'AAAA': 'ipv6',
        'CNAME': 'cname',
        'MX': 'mx',
        'TXT': 'txt',
        'SRV': 'srv',
        'NS': 'ns',
    }

    def active(self, order_id):
        url = '/api/dns/activate.json'
        params = {'order-id': order_id}
        return self._post(url, params)

    def add(self, domain, hostname, type, content, priority, ttl):
        url = '/api/dns/manage/add-%s-record.json' % self.types[type]
        params = {
            'domain-name': domain,
            'host': hostname,
            'value': content,
            'ttl': ttl,
            'priority': priority,
        }
        return self._post(url, params)

    def modify(self, domain, hostname, type, content, new_content,
               priority, ttl):
        url = '/api/dns/manage/update-%s-record.json' % self.types[type]
        params = {
            'domain-name': domain,
            'host': hostname,
            'current-value': content,
            'new-value': new_content,
            'ttl': ttl,
            'priority': priority,
        }
        return self._post(url, params)

    def delete(self, domain, hostname, type, content):
        url = '/api/dns/manage/delete-%s-record.json' % self.types[type]
        params = {
            'domain-name': domain,
            'host': hostname,
            'value': content,
        }
        return self._post(url, params)


class API(object):

    def __init__(self):
        self.domain = Domain(self)
        self.contact = Contact(self)
        self.dns = DNS(self)
        self.http = HTTPClient(base_url=base_url)


def configure_from_settings(settings):
    global base_url
    global userid
    global password
    global customerid
    global defaultns
    global privacy
    global invoiceoption
    base_url = settings['resellbiz.base_url'].strip()
    userid = settings['resellbiz.userid'].strip()
    password = settings['resellbiz.password'].strip()
    customerid = settings['resellbiz.customerid'].strip()
    defaultns = json.loads(settings['resellbiz.defaultns'].strip())
    privacy = settings['resellbiz.privacy'].strip()
    invoiceoption = settings['resellbiz.invoiceoption'].strip()


def includeme(config):
    configure_from_settings(config.registry.settings)
