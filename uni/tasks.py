from flask.ext.mail import Message
from celery import Celery
from uni import mail, config, create_app


celery = Celery('uni')
celery.config_from_object(config.Config)


@celery.task
def send_email(sender, recipients, subject, html):
    app = create_app()
    with app.test_request_context():
        msg = Message(subject, recipients=recipients)
        msg.html = html
        msg.sender = sender
        mail.send(msg)


@celery.task
def daily():
    app = create_app()
    from uni import hosting, invoice
    hosting_api = hosting.API()
    invoice_api = invoice.API()
    with app.test_request_context():
        hosting_api.notify_expired(days=0)
        hosting_api.suspend_expired(days=7)
        hosting_api.delete_expired(days=15)
        invoice_api.delete_unpaid(10)
        hosting_api.notify_usage()
