from datetime import timedelta
from celery.schedules import crontab


class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'lsdkfjlks10231klsdf)!@$*032984'
    '''
    sqlalchemy settings
    '''
    SQLALCHEMY_DATABASE_URI = 'mysql://root:password@localhost/uni'
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_POOL_RECYCLE = 3600
    '''
    flask-babel settings
    '''
    BABEL_DEFAULT_LOCALE = 'zh_CN'
    '''
    flask-cache settings
    '''
    CACHE_TYPE = 'redis'
    CACHE_DEFAULT_TIMEOUT = '86400'
    '''
    mail settings
    '''
    MAIL_SERVER = "smtp.gmail.com"
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = "support@eduoyun.com"
    MAIL_PASSWORD = "a"
    DEFAULT_MAIL_SENDER = "support@eduoyun.com"
    '''
    flask-login settings
    '''
    REMEMBER_COOKIE_DURATION = timedelta(days=30)
    '''
    flask-debugtoolbar settings
    '''
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    '''
    resellbiz config
    '''
    BASE_URL = "https://httpapi.com"
    USERID = "320892"
    PASSWORD = "a"
    CUSTOMERID = "6786187"
    DEFAULTNS = ["ns1.eduoyun.com", "ns2.eduoyun.com",
                 "ns3.eduoyun.com", "ns4.eduoyun.com"]
    PRIVACY = False
    INVOICEOPTION = "KeepInvoice"
    '''
    celery settings
    '''
    BROKER_URL = "redis://localhost:6379/0"
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
    CELERYBEAT_SCHEDULE = {
        'run-every-day': {
            'task': 'uni.tasks.daily',
            'schedule': crontab(hour=0, minute=0),
            'args': ()
        },
    }
    CELERY_TIMEZONE = 'Asia/Shanghai'
    CELERYD_POOL = 'gevent'
    '''
    Vast settings
    '''
    VAST_URL = 'http://localhost:6621'


class ProductionConfig(Config):
    DEBUG = False
    '''
    Vast settings
    '''
    VAST_URL = 'http://vast.eduoyun.com:6621'


class DevelopmentConfig(Config):
    DEBUG = True
    '''
    resellbiz config
    '''
    BASE_URL = "https://test.httpapi.com"
    USERID = "331070"
    PASSWORD = "a"
    CUSTOMERID = "8919792"
    DEFAULTNS = ["ns1.resell.biz", "ns2.resell.biz"]
    PRIVACY = False
    INVOICEOPTION = "KeepInvoice"
    '''
    sqlalchemy settings
    '''
    SQLALCHEMY_ECHO = True
    '''
    Vast settings
    '''
    VAST_URL = 'http://localhost:6621'
