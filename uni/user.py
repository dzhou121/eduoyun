import os
from hashlib import sha1
from itsdangerous import URLSafeTimedSerializer, BadSignature

from flask.ext.login import UserMixin

from uni import cache, app, db
from uni.models import User, UserContact


class API(object):

    def get_by_username(self, username):
        user = db.session.query(User).\
            filter_by(username=username).\
            filter_by(deleted=False).\
            first()
        if user:
            return self._basic(user)
        return {}

    def get_all(self):
        user = db.session.query(User).\
            filter_by(deleted=False).\
            all()
        return user

    @staticmethod
    @cache.memoize(3600)
    def get_by_id(id):
        user = db.session.query(User).\
            filter_by(id=id).\
            first()
        if user is None:
            return None
        else:
            return API._basic(user)

    def load_user(self, user_id):
        user = self.get_by_id(user_id)
        if user is None:
            return None

        return UserLogin(user['id'], user['username'], user['group'])

    def create(self, username, password):
        hashed_pass = self._hash_password(password)
        user = User(username=username, password=hashed_pass)
        db.session.add(user)
        db.session.flush()
        return self._basic(user)

    def validate(self, username, password):
        user = db.session.query(User).\
            filter_by(username=username).\
            filter_by(deleted=False).\
            first()
        if user and self._validate_password(user, password):
            return self._basic(user)

    def change_password(self, username, original_password, new_password):
        user = self.validate(username, original_password)
        if user:
            self.update_password(user['id'], new_password)
            message = 'Password changed'
        else:
            message = 'Wrong password'
        return message

    def update_password(self, user_id, password):
        hashed_pass = self._hash_password(password)
        user = db.session.query(User).filter_by(id=user_id).first()
        user.password = hashed_pass
        db.session.flush()

    def reset_password(self, id, new_password):
        user = db.session.query(User).\
            filter_by(id=id).\
            first()
        hashed_pass = self._hash_password(new_password)
        user.password = hashed_pass
        db.session.flush()

    @staticmethod
    def _basic(user):
        return {"id": user.id,
                "username": user.username,
                "group": user.group, }

    def _validate_password(self, user, password):
        hashed_pass = sha1()
        hashed_pass.update(password + user.password[:40])
        return user.password[40:] == hashed_pass.hexdigest()

    def _hash_password(self, password):
        salt = sha1()
        salt.update(os.urandom(60))
        hash = sha1()
        hash.update(password + salt.hexdigest())
        hashed_pass = salt.hexdigest() + hash.hexdigest()
        return hashed_pass

    def create_pwreset_token(self, user_id):
        user = db.session.query(User).filter_by(id=user_id).first()
        user = {'id': user.id,
                'password': user.password, }
        s = URLSafeTimedSerializer(app.secret_key)
        token = s.dumps(user)
        return token

    def validate_pwreset_token(self, token):
        s = URLSafeTimedSerializer(app.secret_key)
        try:
            expected_user = s.loads(token, max_age=86400)
        except BadSignature:
            return None

        user = db.session.query(User).filter_by(id=expected_user['id']).first()
        if user.password != expected_user['password']:
            return None
        else:
            return user.id

    def get_contacts(self, username):

        from sqlalchemy.orm import scoped_session, sessionmaker
        from sqlalchemy import create_engine

        engine = create_engine('mysql://root:aircon3$%@localhost/uni',
                               convert_unicode=True)
        DBSession = scoped_session(sessionmaker(bind=engine))

        contacts = DBSession.query(UserContact).\
            join(User).\
            filter(UserContact.deleted == 0).\
            filter(User.username == username).\
            all()

        return [{'id': contact.id,
                 'name': contact.name,
                 'company': contact.company,
                 'email': contact.email,
                 'telephone': contact.telephone,
                 'address': contact.address,
                 'city': contact.city,
                 'province': contact.province,
                 'country': contact.country,
                 'postcode': contact.postcode,
                 'default': contact.default, }
                for contact in contacts]

    def add_contact(self, user_id, name, email, company, telephone, address,
                    city, province, country, iso2, telephone_cc, postcode):
        contact = UserContact(user_id=user_id, name=name, email=email,
                              company=company, telephone=telephone,
                              address=address, city=city, province=province,
                              country=country, iso2=iso2,
                              telephone_cc=telephone_cc, postcode=postcode)
        db.session.add(contact)
        db.session.flush()
        return {"id": contact.id}

    def search_user(self, username):
        users = db.session.query(User).\
            filter(User.username.like('%%%s%%' % username)).\
            filter(User.deleted == 0).\
            all()

        return [self._basic(user) for user in users]


class UserLogin(UserMixin):

    def __init__(self, user_id, username, group):
        self.id = user_id
        self.user_id = user_id
        self.username = username
        self.group = group
