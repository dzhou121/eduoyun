(function($) {
    $.cookie = function(key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) { return s; } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
        }
        return null;
    };
})(jQuery);

function save_dns_record(row) {
    var html = row.html();
    var hostname = row.find(".hostname").val().toLowerCase();
    var type = row.find(".type").val().toUpperCase();
    var content = row.find(".content").val().toLowerCase();
    var priority = row.find(".priority").val().toLowerCase();
    var ttl = row.find(".ttl").val().toLowerCase();
    var record_id = row.find(".record-id").val();

    if (hostname == '') {
        hostname = '@';
    }
    if (type == 'MX' && priority == '') {
        priority = '0';
    }

    var data = {
        "hostname": hostname,
        "type": type,
        "content": content,
        "priority": priority,
        "ttl": ttl,
        "record_id": record_id, 
    };

    var url = record_id == 'new' ? 'add_dns' : 'modify_dns';
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function() {
            var s = record_id == 'new' ? '添加中...' : '修改中...';
            row.html('<td style="text-align: center;" colspan="7">' + s + '</td>');
        },
        success: function(data) {
            var s = record_id == 'new' ? '添加成功' : '修改成功';
            if (data['status'] == 'active') {
                row.html('<td style="text-align: center;" colspan="7"><span class="label label-success">' + s + '</span></td>');
                setTimeout(function () {
                    row.html(html);
                    record_id = record_id == 'new' ? data['id'] : record_id;
                    row.find(".record-id").val(record_id);
                    row.find(".hostname").val(hostname);
                    row.find(".type").val(type);
                    row.find(".content").val(content);
                    row.find(".priority").val(priority);
                    row.find(".ttl").val(ttl);
                    var button = row.find(".save-add-dns-record").parent();
                    button.html( '<button style="margin-right: 2px;" class="btn btn-small change-dns-record">修改</button> <button class="btn btn-danger btn-small delete-dns-record">删除</button>');
                }, 1000);
            }
            else {
                row.html('<td style="text-align: center;" colspan="7"><span class="label label-important">错误: ' + data['message'] + '</span> <button type="button" class="btn re-action-dns-record" record-id="' + record_id + '" hostname="' + hostname + '" record-type="' + type + '" content="' + content + '" priority="' + priority + '" ttl="' + ttl + '">确认</button></td>');
            }
        },
        error: function(data) {
            var s = record_id == 'new' ? '添加错误' : '修改错误';
            row.html('<td style="text-align: center;" colspan="7"><span class="label label-important">错误: ' + s + '</span> <button type="button" class="btn re-action-dns-record" record-id="' + record_id + '" hostname="' + hostname + '" record-type="' + type + '" content="' + content + '" priority="' + priority + '" ttl="' + ttl + '">确认</button></td>');
        },
        cache:false,
    }); 
}

function re_action_dns_record(row, button) {
    var html = $("#dns-record-template").html();
    row.html(html);
    row.find(".record-id").val(button.attr('record-id'));
    row.find(".hostname").val(button.attr('hostname'));
    var type = button.attr('record-type');
    row.find(".type").val(type);
    row.find(".content").val(button.attr('content'));
    if (type == 'MX') {
        row.find(".priority").val(button.attr('priority'));
        row.find(".priority").prop("disabled", false);
    }
    row.find(".ttl").val(button.attr('ttl'));
    if (button.attr('record-id') != 'new'){
        row.find("td.action").html('<button style="margin-right: 2px;" class="btn btn-small change-dns-record">修改</button> <button class="btn btn-danger btn-small delete-dns-record">删除</button>');
    }
    //var button = row.find(".save-add-dns-record").parent();
    //button.html( '<button style="margin-right: 2px;" class="btn btn-small btn-success save-add-dns-record">保存</button><button class="btn btn-danger btn-small cancel-dns-record">取消</button>');
}

function delete_dns_record(row) {
    var record_id = row.find(".record-id").val();
    var url = 'delete_dns';
    data = {"record_id": record_id, };
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function() {
            row.html('<td style="text-align: center;" colspan="7">正在删除</td>');
        },
        success: function(data) {
            if (data['status'] == 'Success') {
                row.html('<td style="text-align: center;" colspan="7"><span class="label label-success">删除成功</span></td>');
                setTimeout(function () {
                    row.fadeOut(500, function() { row.remove(); });
                }, 1000);
            }
            else {
                row.html('<td style="text-align: center;" colspan="7"><span class="label label-important">错误: ' + data['message'] + '</span> <button type="button" class="btn re-add-dns-record" record-id="' + record_id + '" hostname="' + hostname + '" record-type="' + type + '" content="' + content + '" priority="' + priority + '" ttl="' + ttl + '">确认</button></td>');
            }
        },
        error: function(data) {
            row.html('<td style="text-align: center;" colspan="7"><span class="label label-important">错误</span></td>');
        },
        cache:false,
    }); 
}

function add_ns() {
    $(document).on('click', '#add-ns', function() {
        var form_body = $("#form-ns .form-body");
        var group = form_body.find(".control-group:first").clone();
        group.find("input:text").val('');
        form_body.append(group);
    });
}

function delete_selected_dns_record() {
    $(document).on('click', '#delete-selected-dns-record', function() {
        $("input:checkbox:checked.record-id").each(function() {
            var row = $(this).parent().parent();
            delete_dns_record(row);
        });
    });
}

function domain_search_typed() {
    $(document).on('keyup', '#domain-search-typed', function() {
        var search = $(this).val();
        var domains = JSON.parse($(this).attr('data-domains'));
        $("#domain-select").html('');
        for (var i in domains) {
            var domain_id = domains[i]['id'];
            if (domains[i]['name'].indexOf(search) !== -1) {
                var domain = domains[i]['name'];
                $("#domain-select").append('<option value="' + domain_id + '">' + domain + '</option>');
            }
        }
    });
}

function add_to_cart(product_id, name, type, item_id, category, term, auth_code) {
    var product = {
        "product_id": product_id,
        "name": name,
        "type": type,
        "auth_code": auth_code,
        "item_id": item_id,
        "category": category,
        "term": term,
    };
    var cart = $.cookie('cart');
    if(cart){
        cart = JSON.parse(cart)["cart"];
        cart.push(product); 
    }
    else {
        cart = new Array(product);
    }
    $.cookie('cart', JSON.stringify({"cart": cart}), {'path': '/'});
}

function domain_transfer() {
    $(document).on('click', '#domain-transfer', function() {
        var transfer = $("#domain-transfer-block");
        var domain_name = transfer.find("#domain-name").val();
        var tld_id = transfer.find("select :selected").attr('tld_id');
        var tld_name = transfer.find("select :selected").attr('tld_name');
        var auth_code = transfer.find("#auth-code").val();
        var term = 1;
        var product = {
            "product_id": tld_id,
            "name": domain_name + '.' + tld_name,
            "type": "transfer_in",
            "auth_code": auth_code,
            "item_id": 0,
            "category": "domain",
            "term": term + 'y',
        };
        var cart = $.cookie('cart');
        if($.cookie('cart')){
            cart = JSON.parse(cart)["cart"];
            cart.push(product); 
        }
        else {
            cart = new Array(product);
        }
        $.cookie('cart', JSON.stringify({"cart": cart}), {'path': '/'});
    });
}

function domain_renew() {
    $(document).on('click', '.domain-renew', function() {
        var row = $(this).parents("tr");
        var domain = row.attr('data-domain');
        var domain_id = row.attr('data-domain-id');
        var tld_id = row.attr('data-tld-id');
        add_to_cart(tld_id, domain, 'renew', domain_id, 'domain', '1y');
    });
}

function domain_renew_selected() {
    $(document).on('click', '#domain-renew-selected', function() {
        $("input:checkbox:checked.domain-id").each(function() {
            var row = $(this).parents("tr");
            var domain = row.attr('data-domain');
            var domain_id = row.attr('data-domain-id');
            var tld_id = row.attr('data-tld-id');
            add_to_cart(tld_id, domain, 'renew', domain_id, 'domain', '1y');
        });
    });
}

function hosting_renew() {
    $(document).on('click', '#hosting-renew', function() {
        var name = $(this).attr('data-hosting-product-name');
        var product_id = $(this).attr('data-hosting-product-id');
        var item_id = $(this).attr('data-hosting-id');
        add_to_cart(product_id, name, 'renew', item_id, 'hosting', '1m');
    });
}

function hosting_upgrade() {
    $(document).on('click', '#hosting-upgrade', function() {
        var select = $("select#hosting-upgrade-select :selected");
        var name = select.attr("data-product-name");
        console.log(name);
        var product_id = select.val();
        var item_id = $(this).attr('data-hosting-id');
        add_to_cart(product_id, name, 'upgrade', item_id, 'hosting', '1m');
    });
}

function add_glue() {
    $(document).on('click', '#add-glue', function() {
        var row = $("#glue-template").html();
        $("#glue-table").prepend('<tr>' + row + '</tr>');
    });
}

$(document).ready(function() {
    $(".select_all").click(function() {
        var checked_status = this.checked;
        var table = $(this).parent().parent().parent().parent();
        var checkboxes = table.find("input:checkbox");
        checkboxes.each(function()
            {
                this.checked = checked_status;
            });
    });

    $(".order").click(function() {
        var product = {
            "product_id": $(this).attr('product_id'),
            "name": $(this).attr('name'),
            "type": "purchase",
            "item_id": 0,
            "category": "hosting",
            "term": "1m",
        };
        var cart = $.cookie('cart');
        if(cart){
            cart = JSON.parse(cart)["cart"];
            cart.push(product); 
        }
        else {
            cart = new Array(product);
        }
        $.cookie('cart', JSON.stringify({"cart": cart}), {'path': '/'});
    });

    $(".cart-delete").click(function() {
        var row = $(this).parents("tr");
        var current_term = row.find(".product").attr('term');
        var name = row.find(".product").attr('name');
        var product_id = row.find(".product").attr('product_id');
        var item_id = row.find(".product").attr('item_id');
        var type = row.find(".product").attr('type');
        var cart = $.cookie('cart');
        var index = null;
        cart = JSON.parse(cart)["cart"];
        for (var i = 0; i < cart.length; i++) {
            if (cart[i]["name"] == name && cart[i]["product_id"] == product_id && cart[i]["item_id"] == item_id && cart[i]["type"] == type && cart[i]["term"] == current_term) {
                index = i;
            }
        }
        if (index !== null) {
            cart.splice(index, 1);
        }
        if(!cart[0]){
            row.replaceWith('<tr><td colspan="5">购物车无物品</td></tr>');
            $("#checkout").attr('disabled', 'disabled');
        }
        else {
            row.replaceWith();
        }
        $.cookie('cart', JSON.stringify({"cart": cart}), {'path': '/'});
        update_total_price();
    });

    if($("#total").length != 0) {
        update_total_price();
    }

    $(".term select").change(function() {
        var row = $(this).parents("tr");
        var price = row.find(":selected").attr("price");
        var discount = row.find(":selected").attr("discount");
        var term = row.find(":selected").attr("term");
        var item_id = row.find(".product").attr('item_id');
        var name = row.find(".product").attr('name');
        var current_term = row.find(".product").attr('term');
        var product_id = row.find(".product").attr('product_id');
        var type = row.find(".product").attr('type');
        var cart = $.cookie('cart');
        var index = null;
        cart = JSON.parse(cart)["cart"];
        for (var i = 0; i < cart.length; i++) {
            if (cart[i]["name"] == name && cart[i]["product_id"] == product_id && cart[i]["item_id"] == item_id && cart[i]["type"] == type && cart[i]["term"] == current_term) {
                cart[i]["term"] = term;
                row.find(".product").attr('term', term);
                break;
            }
        }
        $.cookie('cart', JSON.stringify({"cart": cart}), {'path': '/'});
        row.find(".price").html(price);
        row.find(".discount").html(discount);
        update_total_price();
    });

    $("#hosting select").change(function() {
        var form = $(this).parents("form");
        form.submit();
    });

    $("#pay").click(function() {
        $(this).parents('form').submit();
        $(this).attr('disabled', 'disabled');
        window.setTimeout(function(){$("#payment").click();}, 100);
    });

    $(".validate-form").validate();
//    {
//    rules: {
//            email: {
//                required: true,
//        email: true,
//            },
//        contact_email: {
//            required: true,
//        email: true,
//        },
//        name: {
//            required: true,
//        },
//        telephone: {
//            required: true,
//        },
//        address: {
//            required: true,
//        },
//        postcode: {
//            required: true,
//        },
//        password: {
//            required: true,
//            minlength: 6,
//        },
//        password_confirm: {
//            equalTo: "#password",
//        },
//        terms_and_conditions: {
//            required: true,
//        },
//        ftpusername: {
//            required: true,
//        },
//        ftppassword: {
//            required: true,
//            minlength: 6,
//        },
//        ftppassword_confirm: {
//            equalto: "#ftppassword",
//        },
//        mysqlusername: {
//            required: true,
//            maxlength: 16,
//        },
//        mysqluserpassword: {
//            required: true,
//            minlength: 6,
//        },
//        mysqluserpassword_confirm: {
//            equalto: "#mysqluserpassword",
//        },
//        domain: {
//            required: true,
//        },
//        mysqldbname: {
//            required: true,
//            maxlength: 64,
//        },
//        },
//            messages: {
//                terms_and_conditions: "请勾选此项",
//            },
//    });

    $("#domain-search").submit(function() {
        var domain_name = $("#domain-name").val();
        var csrf = $("#csrf").val();

        $("#domain-availability").html(null);
        var html = '<h4>你选择的域名后缀</h4><hr />';
        html = html + '<table class="table table-striped"><thead><tr><th>域名</th><th>状态</th></tr></thead><tbody>';
        $("#domain-search option:selected").each(function() {
            var tld_id = $(this).attr('tld_id');
            var tld_name = $(this).attr('tld_name');
            html = html + '<tr domain_name="' + domain_name + '" tld_name="' + tld_name + '" tld_id="' + tld_id + '"><td class="domain">' + domain_name + '.' + tld_name + '</td><td class="status">查询中...</td></tr>';
        });
        html = html + '</tbody></table>';

        var html = html + '<hr /><h4>其他域名后缀</h4><hr />';
        html = html + '<table class="table table-striped"><thead><tr><th>域名</th><th>状态</th></tr></thead><tbody>';

        $("#domain-search option:not(:selected)").each(function() {
            var tld_id = $(this).attr('tld_id');
            var tld_name = $(this).attr('tld_name');
            html = html + '<tr domain_name="' + domain_name + '" tld_name="' + tld_name + '" tld_id="' + tld_id + '"><td class="domain">' + domain_name + '.' + tld_name + '</td><td class="status">查询中...</td></tr>';
        });

        html = html + '</tbody></table>';
        $("#domain-availability").append(html);
        
        var cart = $.cookie('cart');
        if (cart) {
            cart = JSON.parse(cart)["cart"];
        }
        else {
            cart = new Array();
        }
        $("#domain-availability table tbody tr").each(function() {
            var row = $(this);
            var tld_id = row.attr('tld_id');
            var tld_name = row.attr('tld_name');

            for (var i = 0; i < cart.length; i++) {
                if (cart[i]["name"] == domain_name + '.' + tld_name && cart[i]["product_id"] == tld_id && cart[i]["type"] == 'purchase' && cart[i]["category"] == 'domain') {
                    row.find(".status").html('<span class="label label-info">已被加入购物车</span>');
                    return;
                }
            }

            $.ajax({
                url: '/domain/search?csrf=' + csrf + '&domain_name=' + domain_name + '&tld_id=' + tld_id, 
                success: function(data) {
                    if (data['availability'] == '可注册'){
                        row.find(".status").html('<span style="font-size: 12px;" class="label label-success">' + data['availability'] + '</span>');
                        var tlds_select = '<select style="width: 60px; margin-right: 10px; margin-left: 20px;">';
                        for (var i = 1; i <= 10; i++) {
                            tlds_select += '<option term="' + i + '">' + i + '年 ' + data['prices'][i.toString()] + '元</option>';
                        }
                        tlds_select += '</select>';
                        row.find(".status").append(tlds_select);
                        row.find(".status").append('<button class="btn btn-primary btn-small order-domain">加入购物车</button>');
                    }
                    else {
                        row.find(".status").html('<span style="font-size: 12px;"  class="label label-important">' + data['availability'] + '</span>');
                    }
                },
                cache: false
            });
        });

        return false;
    });


    $(document).on('click', ".order-domain", function() {
        var status_column = $(this).parent();
        var row = $(this).parent().parent();
        var tld_id = row.attr('tld_id');
        var tld_name = row.attr('tld_name');
        var domain_name = row.attr('domain_name');
        var term = row.find("select :selected").attr('term');
        var product = {"product_id": tld_id,
            "name": domain_name + '.' + tld_name,
            "type": "purchase",
            "item_id": 0,
            "category": "domain",
            "term": term + 'y', };
        var cart = $.cookie('cart');
        if($.cookie('cart')){
            cart = JSON.parse(cart)["cart"];
            cart.push(product); 
        }
        else {
            cart = new Array(product);
        }
        $.cookie('cart', JSON.stringify({"cart": cart}), {'path': '/'});
        status_column.html('<span class="label label-info">已被加入购物车</span>');
    });

    $(document).on('change', "#province", function() {
        var select = $(this);
        var option = $(this).find(":selected");
        var csrf = $("#csrf").val();
        var province = option.val();
        if (province == null) {
            select.parent().find("span").each(function() {
                if ($(this).attr('for') == 'city') {
                    $(this).remove();
                }
            });
            select.parent().find("#city").remove();
        }
        else {
            if (select.parent().find("#city").attr('id') != null) {
                select.parent().find("span").each(function() {
                    if ($(this).attr('for') == 'city') {
                        $(this).remove();
                    }
                });
                select.parent().find("#city").remove();
                var city_html = '<select id="city" name="city" style="width:110px;">';
                $.ajax({
                    url: '/city?province=' + province + '&csrf=' + csrf,
                    success: function(data) {
                        for (var i in data) {
                            city_html += '<option value="' + data[i]['cn'] + '">' + data[i]['cn'] + '</option>';
                        }

                        city_html += '</select>';
                        select.parent().append(city_html);
                    },
                    cache:false,
                }); 
            }
            else {
                var city_html = '<select id="city" name="city" style="width:110px;">';
                $.ajax({
                    url: '/city?province=' + province + '&csrf=' + csrf,
                    success: function(data) {
                        for (var i in data) {
                            city_html += '<option value="' + data[i]['cn'] + '">' + data[i]['cn'] + '</option>';
                        }

                        city_html += '</select>';
                        select.parent().append(city_html);
                    },
                    cache:false,
                }); 
            }
        }
    });

    $(document).on('change', "#country", function() {
        var select = $(this);
        var option = $(this).find(":selected");
        var country = option.val();
        var contact = $(this).parent().parent().parent();
        var csrf = $("#csrf").val();
        if (country != '中国') {
            if (select.parent().find("#province")) {
                select.parent().find("span").each(function() {
                    if ($(this).attr('for') == 'province') {
                        $(this).remove();
                    }
                });
                select.parent().find("#province").remove();
            }
            if (select.parent().find("#city")) {
                select.parent().find("span").each(function() {
                    if ($(this).attr('for') == 'city') {
                        $(this).remove();
                    }
                });
                select.parent().find("#city").remove();
            }
            if (contact.find("#manual-province").attr('id') == null) {
                contact.find("#address").parent().parent().before('<div id="manual-province" class="control-group"> <label class="control-label">省份:</label> <div class="controls"> <input type="text" class="input-large required" name="province" > </div> </div>');
            }
            if (contact.find("#manual-city").attr('id') == null) {
                contact.find("#address").parent().parent().before('<div id="manual-city" class="control-group"> <label class="control-label">城市:</label> <div class="controls"> <input type="text" class="input-large required" name="city"> </div> </div>');
            }
        }
        else {
            if (contact.find("#manual-province")) {
                contact.find("#manual-province").remove();
            }
            if (contact.find("#manual-city")) {
                contact.find("#manual-city").remove();
            }
            var province_html = '<select id="province" class="required" name="province" style="width:110px;"><option value="">-请选择省份-</option>';
            $.ajax({
                url: '/province?csrf=' + csrf,
                success: function(data) {
                    for (var i in data) {
                        province_html += '<option value="' + data[i]['cn'] + '">' + data[i]['cn'] + '</option>';
                    }

                    province_html += '</select>';
                    select.parent().append(province_html);
                },
                cache:false,
            }); 
        }
    });

    $(document).on('click', '#create-contact', function() {
        var div = $(this).parent().parent();
        var csrf = $("#csrf").val();
        $("#existing-contact").hide();
        div.append('<div id="new-contact"><h4 style="margin-bottom:20px;">创建新联系方式:</h4><input type="hidden" name="new_contact" /><div class="control-group"> <label class="control-label">姓名:</label> <div class="controls"> <input type="text" class="input-large" id="name" name="name"> </div> </div> <div class="control-group"> <label class="control-label">公司:</label> <div class="controls"> <input type="text" class="input-large" id="company" name="company"> </div> </div> <div class="control-group"> <label class="control-label">Email:</label> <div class="controls"> <input type="text" class="input-large" id="contact_email" name="contact_email" > </div> </div> <div class="control-group"> <label class="control-label">电话:</label> <div class="controls"> <input type="text" class="input-large" id="telephone" name="telephone" > </div> </div> <div class="control-group"> <div class="controls"> <select id="country" name="country" style="width:80px;"></select> <select id="province" class="required" name="province" style="width:110px;"> <option value="">-请选择省份-</option> </select> </div> </div> <div class="control-group"> <label class="control-label">地址:</label> <div class="controls"> <input type="text" class="input-large" id="address" name="address" > </div> </div> <div class="control-group"> <label class="control-label">邮编:</label> <div class="controls"> <input type="text" class="input-large" id="postcode" name="postcode" > </div> </div><div class="control-group" style="margin-bottom:0"><div class="controls"><a id="cancel-create-contact" href="#">取消创建新联系方式</a></div> </div></div>');

        var country_html = '';
        $.ajax({
            url: '/country?csrf=' + csrf,
            success: function(data) {
                for (var i in data) {
                    if (data[i]['cn'] == '中国') {
                        country_html += '<option selected value="' + data[i]['cn'] + '">' + data[i]['cn'] + '</option>';
                    }
                    else {
                        country_html += '<option value="' + data[i]['cn'] + '">' + data[i]['cn'] + '</option>';
                    }
                }

                div.find("#country").append(country_html);
            },
            cache:false,
        }); 

        var province_html = '';
        $.ajax({
            url: '/province?csrf=' + csrf,
            success: function(data) {
                for (var i in data) {
                    province_html += '<option value="' + data[i]['cn'] + '">' + data[i]['cn'] + '</option>';
                }

                div.find("#province").append(province_html);
            },
            cache:false,
        }); 

    });

    $(document).on('click', '#user-domain-create-contact', function() {
        var form = $('#user-domain-create-contact-form');
        form.show();
        var country_html = '';
        $.ajax({
            url: '/country?csrf=',
            success: function(data) {
                for (var i in data) {
                    if (data[i]['cn'] == '中国') {
                        country_html += '<option selected value="' + data[i]['cn'] + '">' + data[i]['cn'] + '</option>';
                    }
                    else {
                        country_html += '<option value="' + data[i]['cn'] + '">' + data[i]['cn'] + '</option>';
                    }
                }

                form.find("#country").append(country_html);
            },
            cache:false,
        }); 

        var province_html = '';
        $.ajax({
            url: '/province?csrf=',
            success: function(data) {
                for (var i in data) {
                    province_html += '<option value="' + data[i]['cn'] + '">' + data[i]['cn'] + '</option>';
                }

                form.find("#province").append(province_html);
            },
            cache:false,
        }); 

    });
    
    $(document).on('click', '#user-domain-cancel-create-contact', function() {
        var form = $('#user-domain-create-contact-form');
        form.hide();
    });

    $(document).on('click', '#cancel-create-contact', function() {
        var div = $(this).parent().parent().parent().parent();
        $("#new-contact").remove();
        $("#existing-contact").show();
    });

    $(document).on('change', '#domain-select', function() {
        var domain_id = $(this).val();
        var link = $("#domain-select-link");
        link.attr("href", domain_id);
    });

    $(document).on('click', '#add-dns-record', function() {
        var row = $("#dns-record-template").html();
        $("tbody").prepend('<tr>' + row + '</tr>');
    });

    $(document).on('click', '.save-add-dns-record', function() {
        var row = $(this).parent().parent();
        save_dns_record(row);
    });

    $(document).on('change', '.type', function() {
        var type = $(this).val();
        var row = $(this).parent().parent();
        var priority = row.find(".priority");
        if (type == 'MX') {
            priority.prop("disabled", false);
        }
        else {
            priority.val('');
            priority.prop("disabled", true);
        }
    });

    $(document).on('click', '.cancel-dns-record', function() {
        var row = $(this).parent().parent();
        row.remove();
    });

    $(document).on('click', '.change-dns-record', function() {
        var row = $(this).parent().parent();
        save_dns_record(row)
    });

    $(document).on('click', '.delete-dns-record', function() {
        var row = $(this).parent().parent();
        delete_dns_record(row);
    });

    $(document).on('click', '.re-action-dns-record', function() {
        var button = $(this);
        var row = $(this).parent().parent();
        re_action_dns_record(row, button);
    });
    
    $(document).on('click', '.cancel-add-glue', function() {
        var row = $(this).parent().parent();
        row.remove();
    });

    $(document).on('change', '#ror_checkbox', function() {
        var form = $("#toggle_ror_form");
        form.submit();
    });

    $(document).on('click', '#install-gem-button', function() {
        $.ajax({
            type: "POST",
            url: 'install_gem',
            beforeSend: function() {
                $("#install-gem").html('正在安装依赖Gem, 请耐心等待...');
            },
            success: function(data) {
                if (data['resp']['severity'] == 'success') {
                    $("#install-gem").toggleClass('alert-info alert-success');
                    $("#install-gem").html('安装依赖Gem成功');
                    $("#ror-control").show();
                }
                else {
                    $("#install-gem").toggleClass('alert-info alert-error');
                    $("#install-gem").html('安装依赖Gem失败，请联系客服安装');
                }
            },
            error: function(data) {
                $("#install-gem").toggleClass('alert-info alert-error');
                $("#install-gem").html('安装依赖Gem失败，请联系客服安装');
            },
            cache:false,
        }); 
    });
    
    $(document).on('click', '#init-db', function() {
        $("#init-db").addClass('btn-info');
        $("#init-db").html("正在初始化数据库...");
    });
    
    $(document).on('click', '#precompile-assets', function() {
        $("#precompile-assets").addClass('btn-info');
        $("#precompile-assets").html("正在预编译静态文件...");
    });

    delete_selected_dns_record();
    add_ns();
    domain_search_typed();
    domain_transfer();
    domain_renew();
    domain_renew_selected();
    hosting_renew();
    hosting_upgrade();
    add_glue();
    save_add_glue();
    re_add_glue();
    change_glue();
    delete_glue();
});

function save_add_glue() {
    $(document).on('click', '.save-add-glue', function() {
        var row = $(this).parents("tr");
        save_glue(row);
    });
}

function change_glue() {
    $(document).on('click', '.change-glue', function() {
        var row = $(this).parents("tr");
        save_glue(row);
    });
}

function save_glue(row) {
    var html = row.html();
    var hostname = row.find(".hostname").val();
    var ip = row.find(".ip").val();
    var glue_id = row.find(".glue-id").val();
    
    var data = {
        "hostname": hostname,
        "ip": ip,
        "glue_id": glue_id,
    }
    var url = glue_id == 'new' ? 'add_glue' : 'modify_glue';
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        beforeSend: function() {
            var s = glue_id == 'new' ? '添加中...' : '修改中...';
            row.html('<td style="text-align: center;" colspan="4">' + s + '</td>');
        },
        success: function(data) {
            var s = glue_id == 'new' ? '添加成功' : '修改成功';
            if (data['status'] == 'active') {
                row.html('<td style="text-align: center;" colspan="4"><span class="label label-success">' + s + '</span></td>');
                setTimeout(function () {
                    row.html(html);
                    glue_id = glue_id == 'new' ? data['id'] : glue_id;
                    row.find(".glue-id").val(glue_id);
                    row.find(".hostname").val(hostname);
                    row.find(".ip").val(ip);
                    var button = row.find(".save-add-glue").parent();
                    button.html( '<button style="margin-right: 2px;" class="btn btn-small change-glue">修改</button> <button class="btn btn-danger btn-small delete-glue">删除</button>');
                }, 1000);
            }
            else {
                row.html('<td style="text-align: center;" colspan="4"><span class="label label-important">错误: ' + data['message'] + '</span> <button type="button" class="btn re-action-glue" glue-id="' + glue_id + '" hostname="' + hostname + '" ip="' + ip + '">确认</button></td>');
            }
        },
        error: function(data) {
            var s = glue_id == 'new' ? '添加错误' : '修改错误';
            row.html('<td style="text-align: center;" colspan="4"><span class="label label-important">错误: ' + s + '</span> <button type="button" class="btn re-action-glue" glue-id="' + glue_id + '" hostname="' + hostname + '" ip="' + ip + '">确认</button></td>');
        },
        cache:false,
    }); 
}

function re_add_glue() {
    $(document).on('click', '.re-action-glue', function() {
        var button = $(this);
        var row = $(this).parent().parent();
        var html = $("#glue-template").html();
        row.html(html);
        row.find(".glue-id").val(button.attr('glue-id'));
        row.find(".hostname").val(button.attr('hostname'));
        row.find(".ip").val(button.attr('ip'));
        if (button.attr('glue-id') != 'new'){
            row.find("td.action").html('<button style="margin-right: 2px;" class="btn btn-small change-glue">修改</button> <button class="btn btn-danger btn-small delete-glue">删除</button>');
        }
    });
}

function delete_glue() {
    $(document).on('click', '.delete-glue', function() {
        var row = $(this).parents("tr");
        var hostname = row.find(".hostname").val();
        var ip = row.find(".ip").val();
        var glue_id = row.find(".glue-id").val();
        
        var data = {
            "glue_id": glue_id,
        }
        var url = 'delete_glue';
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function() {
                row.html('<td style="text-align: center;" colspan="4">正在删除</td>');
            },
            success: function(data) {
                if (data['status'] == 'Success') {
                    row.html('<td style="text-align: center;" colspan="4"><span class="label label-success">删除成功</span></td>');
                    setTimeout(function () {
                        row.fadeOut(500, function() { row.remove(); });
                    }, 1000);
                }
                else {
                    row.html('<td style="text-align: center;" colspan="4"><span class="label label-important">错误: ' + data['message'] + '</span> <button type="button" class="btn re-action-glue" glue-id="' + glue_id + '" hostname="' + hostname + '" ip="' + ip + '">确认</button></td>');
                }
            },
            error: function(data) {
                var s = '删除错误';
                row.html('<td style="text-align: center;" colspan="4"><span class="label label-important">错误: ' + s + '</span> <button type="button" class="btn re-action-glue" glue-id="' + glue_id + '" hostname="' + hostname + '" ip="' + ip + '">确认</button></td>');
            },
            cache:false,
        }); 
    }); 
}

function update_total_price(){
    var total = 0
        $(".price").each(function(){
            total = total + parseFloat($(this).text());
        });
    $("#total").html(total);
}
