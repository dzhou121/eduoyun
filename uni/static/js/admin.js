$(document).ready(function() {
    $(document).on('click', ".delete-hosting", function() {
        var hosting_id = $(this).parents("tr").find(".hosting-id").val();
        var modal = $("#modal-delete-hosting");
        modal.find("#delete-hosting-confirm").val(hosting_id);
        modal.modal('show');
    });
});
