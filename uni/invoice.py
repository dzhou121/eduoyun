import datetime

from sqlalchemy import func
from uni import db
from uni.models import Invoice, InvoiceItem, User
from uni.models import HostingProduct, DomainTld, UserDomain


class API(object):

    def validate_by_user(self, username, invoice_id):
        invoice = db.session.query(Invoice).\
            join(User).\
            filter(User.username == username).\
            filter(Invoice.id == invoice_id).\
            first()
        if invoice:
            return self._invoice_basic(invoice)
        else:
            return {}

    def get_invoices_by_user(self, username):
        invoices = db.session.query(Invoice, User,
                                    func.sum(InvoiceItem.amount).
                                    label("amount")).\
            filter(Invoice.user_id == User.id).\
            filter(Invoice.id == InvoiceItem.invoice_id).\
            filter(User.username == username).\
            filter(Invoice.deleted == 0).\
            filter(InvoiceItem.deleted == 0).\
            group_by(Invoice.id).\
            order_by(Invoice.id.desc()).\
            all()
        return [self._invoice_whole(invoice, amount)
                for invoice, user, amount in invoices]

    def get_unpaid_invoices_by_user(self, username):
        invoices = db.session.query(Invoice, User,
                                    func.sum(InvoiceItem.amount).
                                    label("amount")).\
            filter(Invoice.user_id == User.id).\
            filter(Invoice.id == InvoiceItem.invoice_id).\
            filter(User.username == username).\
            filter(Invoice.deleted == 0).\
            filter(InvoiceItem.deleted == 0).\
            filter(Invoice.paid == 0).\
            group_by(Invoice.id).\
            order_by(Invoice.id.desc()).\
            all()
        return [self._invoice_whole(invoice, amount)
                for invoice, user, amount in invoices]

    def get_by_uuid(self, uuid):
        invoice = db.session.query(Invoice).\
            filter_by(uuid=uuid).\
            first()
        return self._invoice_basic(invoice)

    def _invoice_whole(self, invoice, amount):
        return {"id": invoice.id,
                "uuid": invoice.uuid,
                "amount": amount,
                "created_at": invoice.created_at,
                "paid": invoice.paid,
                "paid_at": invoice.paid_at, }

    def get_items_by_invoice_id(self, invoice_id):
        items = []
        invoice_items = db.session.query(InvoiceItem).\
            filter_by(deleted=False).\
            filter_by(invoice_id=invoice_id).\
            all()
        for invoice_item in invoice_items:
            if invoice_item.category == 'hosting':
                hosting_product = db.session.query(HostingProduct).\
                    filter_by(id=invoice_item.product_id).\
                    first()
                items.append(self._item_basic(invoice_item,
                                              hosting_product.name))
            elif invoice_item.category == 'domain':
                tld = db.session.query(DomainTld).\
                    filter_by(id=invoice_item.product_id).\
                    first()
                user_domain = db.session.query(UserDomain).\
                    filter_by(id=invoice_item.item_id).\
                    first()
                items.append(self._item_basic(invoice_item,
                                              '%s.%s' % (user_domain.name,
                                                         tld.name)))

        return items

    def create(self, user_id, voucher_id):
        invoice = Invoice(user_id=user_id, voucher_id=voucher_id)
        db.session.add(invoice)
        db.session.flush()
        uuid = "%s%s" % (datetime.datetime.now().strftime("%Y%m%d"),
                         str(invoice.id))
        invoice.uuid = uuid
        db.session.flush()
        return self._invoice_basic(invoice)

    def create_item(self, invoice_id, item_id, product_id, type,
                    price, discount, term, category):
        invoice_item = InvoiceItem(invoice_id=invoice_id,
                                   item_id=item_id,
                                   product_id=product_id,
                                   type=type,
                                   term=term,
                                   category=category,
                                   amount=price,
                                   discount=discount)
        db.session.add(invoice_item)
        db.session.flush()

    def _invoice_basic(self, invoice):
        return {"id": invoice.id,
                "uuid": invoice.uuid,
                "user_id": invoice.user_id,
                "paid": invoice.paid,
                "paid_at": invoice.paid_at, }

    def _item_basic(self, invoice_item, product_name):
        return {"term": invoice_item.term,
                "amount": invoice_item.amount,
                "type": invoice_item.type,
                "item_id": invoice_item.item_id,
                "product_id": invoice_item.product_id,
                "discount": invoice_item.discount,
                "category": invoice_item.category,
                "product": product_name, }

    def mark_as_paid(self, id):
        invoice = db.session.query(Invoice).\
            filter_by(id=id).\
            first()
        invoice.paid = True
        invoice.paid_at = datetime.datetime.now()
        db.session.flush()

    def delete_unpaid(self, days):
        self.delete_unpaid_upgrade()
        invoices = db.session.query(Invoice).\
            filter_by(deleted=False).\
            filter_by(paid=False).\
            filter(Invoice.created_at < datetime.datetime.now() -
                   datetime.timedelta(days=days)).\
            all()
        for invoice in invoices:
            invoice_items = db.session.query(InvoiceItem).\
                filter_by(invoice_id=invoice.id).\
                filter_by(deleted=0).\
                all()
            for invoice_item in invoice_items:
                invoice_item.deleted = True
                invoice_item.deleted_at = datetime.datetime.now()
            invoice.deleted = True
            invoice.deleted_at = datetime.datetime.now()
        db.session.commit()

    def delete_unpaid_upgrade(self):
        invoice_items = db.session.query(InvoiceItem).\
            join(Invoice, Invoice.id == InvoiceItem.invoice_id).\
            filter(InvoiceItem.deleted == 0).\
            filter(InvoiceItem.category == 'hosting').\
            filter(InvoiceItem.type == 'upgrade').\
            filter(Invoice.paid == 0).\
            filter(Invoice.deleted == 0).\
            filter(InvoiceItem.created_at < datetime.datetime.now() -
                   datetime.timedelta(days=1)).\
            all()

        for invoice_item in invoice_items:
            invoice_item.deleted = True
            invoice_item.deleted_at = datetime.datetime.now()

        db.session.commit()

    def get_monthly_sales(self):
        monthly_sales = db.session.query(func.sum(InvoiceItem.amount),
                                         func.year(Invoice.paid_at),
                                         func.month(Invoice.paid_at)).\
            join(Invoice, Invoice.id == InvoiceItem.invoice_id).\
            filter(Invoice.paid == 1).\
            group_by(func.year(Invoice.paid_at)).\
            group_by(func.month(Invoice.paid_at)).\
            all()

        return [{
            'amount': amount,
            'year': year,
            'month': month,
        } for amount, year, month in monthly_sales]
