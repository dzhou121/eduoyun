from logging import Formatter
from logging.handlers import TimedRotatingFileHandler

file_handler = TimedRotatingFileHandler('/var/log/uni/uni.log',
                                        when='midnight',
                                        interval=1)
file_handler.setFormatter(
    Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
)
