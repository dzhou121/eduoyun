import httplib2
from uni import app

try:
    import simplejson as json
except ImportError:
    import json


vast_url = app.config['VAST_URL']


class HTTPClient(object):

    def __init__(self, headers, base_url, host):
        self.h = httplib2.Http()
        self.headers = headers
        self.base_url = base_url
        self.host = host

    def _request(self, url, method, **kwargs):
        base_url = self.base_url
        if 'body' in kwargs:
            kwargs['body'] = json.dumps(kwargs['body'])
        if url[:4] == 'host':
            base_url = self.host
            url = url[4:]
        # print base_url + url, method, self.headers, kwargs

        try:
            resp, body = self.h.request(base_url + url,
                                        method,
                                        headers=self.headers,
                                        **kwargs)
        except:
            body = None

        if body:
            try:
                body = json.loads(body)
            except ValueError:
                body = {"badMethod": {"message": "Unknown Error"}}
        else:
            body = None

        return body

    def get(self, url):
        return self._request(url, 'GET')

    def post(self, url, body):
        return self._request(url, 'POST', body=body)

    def put(self, url, body):
        return self._request(url, 'PUT', body=body)

    def delete(self, url):
        return self._request(url, 'DELETE')


class Client(object):

    def __init__(self, osuser_id=None):
        self.h = httplib2.Http()
        self.headers = {"Content-Type": "application/json",
                        "X-Auth-User": "test",
                        "X-Auth-Password": "test", }
        self.host = vast_url
        self.base_url = "%s/osusers/%s" % (self.host, osuser_id)
        self.osusers = Osuser(self)
        self.websites = Website(self)
        self.ftpusers = Ftpuser(self)
        self.mysqlusers = Mysqluser(self)
        self.mysqldbs = Mysqldb(self)
        self.apptypes = Apptype(self)
        self.apps = App(self)
        self.usages = Usage(self)
        self.websitememberships = Websitemembership(self)
        self.mysqlmemberships = Mysqlmembership(self)
        self.client = HTTPClient(headers=self.headers,
                                 base_url=self.base_url,
                                 host=self.host)


class ClientManager(object):

    def __init__(self, api):
        self.api = api

    def _list(self, url):
        return self.api.client.get(url)

    def _show(self, url):
        return self.api.client.get(url)

    def _create(self, url, body):
        return self.api.client.post(url, body)

    def _update(self, url, body):
        return self.api.client.put(url, body)

    def _delete(self, url):
        return self.api.client.delete(url)


class Osuser(ClientManager):

    def create(self, username):
        body = {"osuser": {"username": username}}
        return self._create("host/osusers", body=body)

    def show(self, id):
        return self._show("host/osusers/%s" % id)

    def delete(self, id):
        return self._delete("host/osusers/%s" % id)

    def suspend(self, id):
        body = {"suspend": ''}
        return self._update("host/osusers/%s" % id, body=body)

    def resume(self, id):
        body = {"resume": ''}
        return self._update("host/osusers/%s" % id, body=body)


class Website(ClientManager):

    def list(self):
        return self._list("/websites")

    def show(self, id):
        return self._show("/websites/%s" % id)

    def create(self, name, admin_email):
        body = {"website": {"name": name,
                "admin_email": admin_email, }}
        return self._create("/websites", body=body)

    def delete(self, id):
        return self._delete("/websites/%s" % id)

    def display(self, id, path):
        body = {"display": path}
        return self._update("/websites/%s" % id, body=body)

    def zip(self, id, path, entries, archname):
        body = {"zip": {"path": path,
                        "entries": entries,
                        "archname": archname, }}
        return self._update("/websites/%s" % id, body=body)

    def unzip(self, id, path, archname):
        body = {"unzip": {"path": path,
                          "archname": archname, }}
        return self._update("/websites/%s" % id, body=body)

    def delete_files(self, id, path, entries):
        body = {"delete": {"path": path,
                           "entries": entries, }}
        return self._update("/websites/%s" % id, body=body)

    def download(self, id, path, file):
        body = {"download": {"path": path,
                             "file": file, }}
        return self._update("/websites/%s" % id, body=body)

    def upload(self, id, path, file, file_name):
        body = {
            "upload": {
                "path": path,
                "file": file,
                "file_name": file_name,
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def change_ror_path(self, id, path):
        body = {
            "change_ror_path": {
                "path": path,
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def check_ror_path(self, id):
        body = {
            "check_ror_path": {
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def get_ror_path(self, id):
        body = {
            "get_ror_path": {
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def check_ror_gem(self, id):
        body = {
            "check_ror_gem": {
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def check_ror_status(self, id):
        body = {
            "check_ror_status": {
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def restart_ror(self, id):
        body = {
            "restart_ror": {
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def enable_ror(self, id, path):
        body = {
            "enable_ror": {
                'path': path,
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def disable_ror(self, id):
        body = {
            "disable_ror": {
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def ror_init_db(self, id):
        body = {
            "ror_init_db": {
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def ror_precompile_assets(self, id):
        body = {
            "ror_precompile_assets": {
            }
        }
        return self._update("/websites/%s" % id, body=body)

    def install_gem(self, id):
        body = {
            "install_gem": {
            }
        }
        return self._update("/websites/%s" % id, body=body)


class Ftpuser(ClientManager):

    def list(self):
        return self._list("/ftpusers")

    def show(self, id):
        return self._show("/ftpusers/%s" % id)

    def create(self, username, password, admin):
        body = {"ftpuser": {"username": username,
                            "password": password,
                            "admin": admin, }}
        return self._create("/ftpusers", body=body)

    def change_password(self, id, password):
        body = {"change_password": {"password": password}}
        return self._update("/ftpusers/%s" % id, body=body)

    def delete(self, id):
        return self._delete("/ftpusers/%s" % id)


class Websitemembership(ClientManager):

    def list(self):
        return self._list("/websitememberships")

    def show(self, id):
        return self._show("/websitememberships/%s" % id)

    def create(self, ftpuser_id, website_id):
        body = {"websitemembership": {"ftpuser_id": ftpuser_id,
                                      "website_id": website_id, }}
        return self._create("/websitememberships", body=body)

    def delete(self, id):
        return self._delete("/websitememberships/%s" % id)


class App(ClientManager):

    def list(self, website_id):
        return self._list("host/websites/%s/apps" % website_id)
        url = "%swebsites/%s/apps" % (self.host, website_id)
        resp, content = self.h.request(url, "GET", headers=self.headers)
        return dict(resp=resp, content=content)

    def show(self, app_id, website_id):
        url = "%swebsites/%s/apps/%s" % (self.host, website_id, app_id)
        resp, content = self.h.request(url, "GET", headers=self.headers)
        return dict(resp=resp, content=content)

    def create(self, name, path, website_id):
        body = {"app": {"name": name,
                        "path": path, }}
        return self._create("host/websites/%s/apps" % website_id, body=body)

    def delete(self, app_id, website_id):
        return self._delete("host/websites/%s/apps/%s" % (website_id, app_id))


class Apptype(ClientManager):

    def list(self):
        return self._list("host/apps/types")


class Mysqluser(ClientManager):

    def list(self):
        return self._list("/mysqlusers")

    def show(self, id):
        return self._show('/mysqlusers/%s' % id)

    def create(self, username, password):
        body = {"mysqluser": {"username": username,
                              "password": password, }}
        return self._create("/mysqlusers", body=body)

    def change_password(self, id, password):
        body = {"change_password": {"password": password}}
        return self._update("/mysqlusers/%s" % id, body=body)

    def delete(self, id):
        return self._delete("/mysqlusers/%s" % id)


class Mysqldb(ClientManager):

    def list(self):
        return self._list("/mysqldbs")

    def show(self, id):
        return self._show('/mysqldbs/%s' % id)

    def create(self, name, charset):
        body = {"mysqldb": {"name": name,
                            "charset": charset, }}
        return self._create("/mysqldbs", body=body)

    def delete(self, id):
        return self._delete("/mysqldbs/%s" % id)


class Mysqlmembership(ClientManager):

    def list(self):
        return self._list('/mysqlmemberships')

    def show(self, id):
        return self._show('/mysqlmemberships/%s' % id)

    def create(self, mysqluser_id, mysqldb_id):
        body = {"mysqlmembership": {"mysqluser_id": mysqluser_id,
                                    "mysqldb_id": mysqldb_id, }}
        return self._create("/mysqlmemberships", body=body)

    def delete(self, id):
        return self._delete("/mysqlmemberships/%s" % id)


class Usage(ClientManager):

    def list(self):
        return self._list("/usages")


def configure_from_settings(settings):
    if 'vast_url' in settings:
        global vast_url
        vast_url = settings['vast_url'].strip()


def includeme(config):
    settings = config.registry.settings
    configure_from_settings(settings)
