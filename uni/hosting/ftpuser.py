from uni.vastclient.client import Client


class API(object):

    def __init__(self, osuser_id=None):
        self.client = Client(osuser_id)
    
    def create_ftpuser(self, ftpusername, ftppassword, admin):
        resp = self.client.ftpusers.create(ftpusername, ftppassword, admin)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if 'ftpuser' in resp:
            response = {
                "severity": "success",
                "message": "ftpuser is created successfully", 
            }
        elif 'badMethod' in resp:
            response = {
                "severity": "error",
                "message": resp['badMethod']['message']
            }
        return response

    def get(self, ftpuser_id):
        return self.client.ftpusers.show(ftpuser_id).get('ftpuser')

    def get_all(self):
        return self.client.ftpusers.list().get('ftpusers')
    
    def get_all_websitememberships(self):
        return self.client.websitememberships.list().\
                get('websitememberships')

    def delete_ftpuser(self, ftpuser_id):
        resp = self.client.ftpusers.delete(ftpuser_id)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if not resp:
            response = {
                "severity": "info",
                "message": "ftpuser is being deleted", 
            }
        else:
            if 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response

    def create_websitemembership(self, ftpuser_id, website_id):
        resp = self.client.websitememberships.create(ftpuser_id, website_id)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if 'websitemembership' in resp:
            response = {
                "severity": "success",
                "message": "website membership is created successfully", 
            }
        elif 'badMethod' in resp:
            response = {
                "severity": "error",
                "message": resp['badMethod']['message']
            }
        return response
    
    
    def delete_websitemembership(self, ftpuser_id, website_id):
        websitememberships = self.client.websitememberships.list().\
                get('websitememberships')
    
        response = {"severity": "error",
                    "message": "Unknown Error", }
        for websitemembership in websitememberships:
            if websitemembership['ftpuser_id'] == ftpuser_id \
               and website_id == websitemembership['website_id']:
                websitemembership_id = websitemembership['id']
                resp = self.client.websitememberships.delete(websitemembership_id)
                if not resp:
                    response = {
                        "severity": "info",
                        "message": "website membership is being deleted", 
                    }
                else:
                    if 'badMethod' in resp:
                        response = {
                            "severity": "error",
                            "message": resp['badMethod']['message']
                        }
        return response
    
    def change_password(self, ftpuser_id, password):
        resp = self.client.ftpusers.change_password(ftpuser_id, password)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if not resp:
            response = {
                "severity": "info",
                "message": "password changed successfully", 
            }
        else:
            if 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response
