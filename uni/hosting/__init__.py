# coding=utf-8

import datetime
from decimal import Decimal
from dateutil.relativedelta import relativedelta
from flask import session
from sqlalchemy import distinct

from uni import utils, cache, db
from uni.tasks import send_email
from uni.models import UserHosting, User, HostingProduct
from uni.models import HostingPrice, InvoiceItem, Invoice
from uni.vastclient.client import Client


class API(object):

    def create(self, user_id, product_id):
        hosting = UserHosting(product_id=product_id,
                              user_id=user_id,
                              status='pending_payment')
        db.session.add(hosting)
        db.session.flush()
        return self._basic(hosting)

    def trial_validate(self, username):
        hosting = db.session.query(UserHosting).\
            join(User).\
            filter(User.username == username).\
            filter(User.deleted == 0).\
            filter(UserHosting.status != 'pending_payment').\
            first()
        if hosting:
            return False
        else:
            return True

    def _basic(self, hosting):
        return {
            "id": hosting.id,
            "product_id": hosting.product_id,
            "user_id": hosting.user_id,
        }

    def _update(self, id, values):
        hosting = db.session.query(UserHosting).\
            filter_by(id=id).\
            first()
        hosting.update(values)
        db.session.flush()
        return self._basic(hosting)

    def active(self, id, term):
        hosting = self.get_by_id(id)
        user = db.session.query(User).filter_by(id=hosting.user_id).first()
        client = Client()
        osuser_username = utils.generate_username(10)
        osuser = client.osusers.create(osuser_username).get('osuser')
        if term[-1] == 'd':
            days = int(term[:-1])
            expires_at = datetime.datetime.now() + relativedelta(days=+days)
        elif term[-1] == 'm':
            months = int(term[:-1])
            expires_at = datetime.datetime.now() + \
                relativedelta(months=+months)
        elif term[-1] == 'y':
            years = int(term[:-1])
            expires_at = datetime.datetime.now() + relativedelta(years=+years)
        values = {
            "status": osuser["status"],
            "osuser_id": osuser['id'],
            "expires_at": expires_at,
        }
        self._update(id, values)
        self.clear_cache_all(user.id)

    def get_by_id(self, id):
        hosting = db.session.query(UserHosting).\
            filter_by(id=id).\
            first()
        return hosting

    def change_expiry(self, id, expires_at):
        values = {"expires_at": expires_at, }
        self._update(id, values)

    def renew(self, item_id, term):
        hosting = self.get_by_id(item_id)
        user = db.session.query(User).filter_by(id=hosting.user_id).first()
        if hosting.status == 'suspended':
            client = Client()
            result = client.osusers.resume(hosting.osuser_id)
            if result:
                return False
        if term[-1] == 'm':
            months = int(term[:-1])
            values = {"expires_at": hosting.expires_at +
                      relativedelta(months=+months),
                      "status": "active", }
            hosting.update(values)
        elif term[-1] == 'y':
            years = int(term[:-1])
            values = {"expires_at": hosting.expires_at +
                      relativedelta(years=+years),
                      "status": "active", }
            hosting.update(values)
        db.session.flush()
        self.clear_cache_all(user.id)

    def upgrade(self, item_id, term, product_id):
        hosting = self.get_by_id(item_id)
        user = db.session.query(User).filter_by(id=hosting.user_id).first()
        if hosting.status == 'suspended':
            client = Client()
            result = client.osusers.resume(hosting.osuser_id)
            if result:
                return False
        if term[-1] == 'm':
            months = int(term[:-1])
            values = {"expires_at": datetime.datetime.now() +
                      relativedelta(months=+months),
                      "status": "active",
                      "product_id": product_id, }
            hosting.update(values)
        elif term[-1] == 'y':
            years = int(term[:-1])
            values = {"expires_at": datetime.datetime.now() +
                      relativedelta(years=+years),
                      "status": "active",
                      "product_id": product_id, }
            hosting.update(values)
        db.session.flush()
        self.clear_cache_all(user.id)

    def get_current(self, user_id):
        hostings = self.get_all(user_id)
        if not hostings:
            return None

        current_hosting = None
        current_hosting_id = session.get('current_hosting_id', None)
        for hosting in hostings:
            if hosting['id'] == current_hosting_id:
                current_hosting = hosting

        if current_hosting is None:
            current_hosting = hostings[0]

        return current_hosting

    @staticmethod
    @cache.memoize(3600)
    def get_all(user_id):
        hostings = db.session.query(UserHosting).\
            filter(UserHosting.user_id == user_id).\
            filter(UserHosting.deleted == 0).\
            filter(UserHosting.status != "pending_payment").\
            all()
        hostings = [API._hosting_whole(hosting) for hosting in hostings]
        return hostings

    @staticmethod
    @cache.memoize(3600)
    def get_app_types():
        client = Client()
        apptypes = client.apptypes.list().get('types')
        return apptypes

    def get_expired(self, days=0, suspended=False):
        if suspended:
            hostings = db.session.query(UserHosting).\
                filter(UserHosting.deleted == 0).\
                filter(UserHosting.expires_at < datetime.datetime.now() -
                       datetime.timedelta(days=days)).\
                filter(UserHosting.status == 'suspended').all()
        else:
            hostings = db.session.query(UserHosting).\
                filter(UserHosting.deleted == 0).\
                filter(UserHosting.expires_at < datetime.datetime.now() -
                       datetime.timedelta(days=days)).\
                filter(UserHosting.status == 'active').all()
        return hostings

    def resume(self, id):
        hosting = self.get_by_id(id)
        if hosting.status == 'suspended':
            user = db.session.query(User).filter_by(id=hosting.user_id).first()
            client = Client()
            result = client.osusers.resume(hosting.osuser_id)
            if not result:
                values = {"status": "active"}
                hosting.update(values)
                db.session.flush()
                self.clear_cache_all(user.id)

    def suspend_expired(self, days=7, mailer=None):
        hostings = self.get_expired(days=days, suspended=False)
        for hosting in hostings:
            user = db.session.query(User).filter_by(id=hosting.user_id).first()
            client = Client()
            client.osusers.suspend(hosting.osuser_id)
            values = {"status": "suspended"}
            hosting.update(values)
            db.session.commit()
            self.clear_cache_all(user.id)
            email = user.username
            url = "https://www.eduoyun.com/user/hosting"
            subject = u'虚拟主机暂停服务 - 易多云'
            body = u"<p>亲爱的会员，</p>"
            body += u"<p>你的虚拟主机已经过期超过7天没有续费了，" + \
                    u"现在我们暂停了该服务，如果你想继续使用，请立刻续费，" + \
                    u"过期超过15天我们会删除所有的网站内容和数据库。</p>"
            body += u"<p>点击以下链接进行续费：</p>"
            body += u'<a href="%s">%s</a>' % (url, url)
            sender = '易多云 客户支持 <support@eduoyun.com>'
            try:
                send_email(sender, [email], subject, body)
            except:
                pass

    def delete(self, user_id, hosting_id):
        hosting = db.session.query(UserHosting).\
            filter(UserHosting.id == hosting_id).\
            first()
        client = Client()
        client.osusers.delete(hosting.osuser_id)
        values = {"deleted": True,
                  "deleted_at": datetime.datetime.now(), }
        hosting.update(values)
        db.session.commit()
        self.clear_cache_all(user_id)

    def delete_expired(self, days=15, mailer=None):
        hostings = self.get_expired(days=days, suspended=True)
        for hosting in hostings:
            user = db.session.query(User).filter_by(id=hosting.user_id).first()
            client = Client()
            client.osusers.delete(hosting.osuser_id)
            values = {"deleted": True,
                      "deleted_at": datetime.datetime.now(), }
            hosting.update(values)
            db.session.commit()
            self.clear_cache_all(user.id)
            email = user.username
            subject = u'虚拟主机删除 - 易多云'
            body = u"<p>亲爱的会员，</p>"
            body += u"<p>你的虚拟主机已经过期超过15天没有续费了，" + \
                    u"所以我们不得不删除了所有的网站内容和数据库，" + \
                    u"我们欢迎你重新购买我们的虚拟主机。</p>"
            sender = '易多云 客户支持 <support@eduoyun.com>'
            try:
                send_email(sender, [email], subject, body)
            except:
                pass

    def notify_expired(self, days=0, mailer=None):
        hostings = self.get_expired(days=days, suspended=False)
        for hosting in hostings:
            expired_time = datetime.datetime.now() - hosting.expires_at
            if expired_time < datetime.timedelta(seconds=86400):
                user = db.session.query(User).\
                    filter_by(id=hosting.user_id).first()
                email = user.username
                url = "https://www.eduoyun.com/user/hosting"
                subject = u'虚拟主机过期 - 易多云'
                body = u"<p>亲爱的会员，</p>"
                body += u"<p>你的虚拟主机已经过期了，" + \
                        u"请立刻续费，过期7天后我们会暂停该服务，" + \
                        u"过期超过15天我们会删除所有的网站内容和数据库。</p>"
                body += u"<p>点击以下链接进行续费：</p>"
                body += u'<a href="%s">%s</a>' % (url, url)
                sender = '易多云 客户支持 <support@eduoyun.com>'
                try:
                    send_email(sender, [email], subject, body)
                except:
                    pass

    def clear_cache_all(self, user_id):
        cache.delete_memoized(self.get_all, user_id)

    @staticmethod
    def _hosting_whole(hosting):
        product = db.session.query(HostingProduct).\
            filter(HostingProduct.id == hosting.product_id).\
            first()
        available_upgrades = db.session.query(HostingProduct).\
            filter(HostingProduct.order > product.order).\
            all()
        available_upgrades = [{"name": available_upgrade.name,
                               "product_id": available_upgrade.id, }
                              for available_upgrade in available_upgrades]
        return {"id": hosting.id,
                "osuser_id": hosting.osuser_id,
                "status": hosting.status,
                "expires_at": hosting.expires_at,
                "product": product.name,
                "product_id": product.id,
                "available_upgrades": available_upgrades,
                "num_websites": product.num_websites,
                "num_mysqldbs": product.num_mysqldbs,
                "bandwidth": product.bandwidth,
                "disk": product.disk, }

    def _get_severity(self, percentage):
        if percentage < 70:
            return ""
        elif percentage >= 70 and percentage < 90:
            return "warning"
        else:
            return "danger"

    def get_usage(self, hosting):
        client = Client(hosting['osuser_id'])
        usage = client.usages.list().get('usage')
        usage['pct_webistes'] = int(100 * usage['num_websites'] /
                                    (hosting['num_websites'] or 100000))
        usage['pct_mysqldbs'] = int(100 * usage['num_mysqldbs'] /
                                    (hosting['num_mysqldbs'] or 100000))
        usage['pct_bandwidth'] = int(100 * usage['bandwidth'] /
                                     (hosting['bandwidth'] or 100000))
        usage['pct_disk'] = int(100 * usage['disk'] /
                                (hosting['disk'] or 100000))
        usage['sevy_websites'] = self._get_severity(usage['pct_webistes'])
        usage['sevy_mysqldbs'] = self._get_severity(usage['pct_mysqldbs'])
        usage['sevy_bandwidth'] = self._get_severity(usage['pct_bandwidth'])
        usage['sevy_disk'] = self._get_severity(usage['pct_disk'])
        expires_in = hosting['expires_at'] - datetime.datetime.now()
        expires_in = expires_in.days + 1
        if expires_in < 0:
            expires_in = 0
        usage['expires_in'] = expires_in
        usage['pct_expires'] = (30 - expires_in) * 100 / 30
        usage['sevy_expires'] = self._get_severity(usage['pct_expires'])
        return usage

    def get_ssh_info(self, hosting):
        client = Client(hosting['osuser_id'])
        osuser = client.osusers.show(hosting['osuser_id']).get('osuser')
        return {
            'username': osuser['username'],
            'password': osuser['password'],
        }

    def notify_usage(self):
        users = db.session.query(distinct(UserHosting.user_id)).\
            filter(UserHosting.deleted == 0).\
            filter(UserHosting.status == 'active')

        for user in users:
            user_id = user[0]
            user_ref = db.session.query(User).\
                filter(User.id == user_id).\
                first()
            username = user_ref.username
            hostings = self.get_all(user_id)
            for hosting in hostings:
                usage = self.get_usage(hosting)
                if usage['pct_bandwidth'] > 99:
                    email = 'dzhou121@gmail.com'
                    subject = u'流量提醒 - 易多云'
                    body = "<p>用户ID: %s</p>" % user_id
                    body += "<p>用户Email: %s</p>" % username
                    body += "<p>流量使用: %s<p>%%" % usage['pct_bandwidth']
                    sender = '易多云 客户支持 <support@eduoyun.com>'
                    send_email.delay(sender, [email], subject, body)

                if usage['pct_disk'] > 99:
                    email = 'dzhou121@gmail.com'
                    subject = u'磁盘空间提醒 - 易多云'
                    body = "<p>用户ID: %s</p>" % user_id
                    body += "<p>空间使用: %s<p>" % usage['pct_disk']
                    sender = '易多云 客户支持 <support@eduoyun.com>'
                    send_email.delay(sender, [email], subject, body)

    def get_prices_by_name(self, name, voucher, type, item_id):
        prices = db.session.query(HostingPrice).\
            join(HostingProduct).\
            filter(HostingProduct.name == name).\
            filter(HostingProduct.deleted == 0).\
            filter(HostingPrice.deleted == 0).\
            all()

        if type == 'upgrade':
            refund_amount = self.get_refund_for_upgrade(item_id)
        else:
            refund_amount = Decimal('0.00')

        return [self._price_basic(price, voucher, refund_amount)
                for price in prices]

    def get_refund_for_upgrade(self, item_id):
        old, expires_at = \
            db.session.query(InvoiceItem, UserHosting.expires_at).\
            join(UserHosting, UserHosting.id == InvoiceItem.item_id).\
            join(Invoice,
                 Invoice.id == InvoiceItem.invoice_id).\
            filter(InvoiceItem.item_id == item_id).\
            filter(InvoiceItem.category == 'hosting').\
            filter(Invoice.paid == 1).\
            filter(InvoiceItem.deleted == 0).\
            filter(Invoice.deleted == 0).\
            filter(UserHosting.deleted == 0).\
            order_by(Invoice.paid_at.desc()).\
            first()
        old_price = old.amount
        term = old.term
        if term[-1] == 'd':
            days = int(term[:-1])
            start_at = expires_at - relativedelta(days=+days)
        elif term[-1] == 'm':
            months = int(term[:-1])
            start_at = expires_at - relativedelta(months=+months)
        elif term[-1] == 'y':
            years = int(term[:-1])
            start_at = expires_at - relativedelta(years=+years)

        term_days = (expires_at - start_at).days
        elapsed_days = (datetime.datetime.now() - start_at).days
        if elapsed_days < 0:
            elapsed_days = 0
        if elapsed_days > term_days:
            elapsed_days = term_days

        refund_amount = Decimal("%.2f" % ((term_days - elapsed_days) *
                                old_price / term_days))

        return refund_amount

    def _price_basic(self, price, voucher, refund_amount):
        discount = refund_amount
        for voucher_item in voucher:
            if voucher_item['price_id'] == price.id and \
               voucher_item['category'] == 'hosting' and \
               voucher_item['numbers'] > 0:
                discount = Decimal(str(voucher_item['discount']))
                voucher_item['numbers'] -= 1
        term = price.term
        price = price.price
        if price - discount < 0:
            discount = price
            price = Decimal('0.00')
        else:
            price = price - discount
        return {"term": term,
                "price": price,
                "discount": discount, }

    def get_product_by_name(self, name):
        product = db.session.query(HostingProduct).\
            filter_by(name=name).\
            filter_by(deleted=False).\
            first()
        return product and self._product_basic(product) or {}

    def _product_basic(self, product):
        return {"id": product.id,
                "name": product.name, }

    @staticmethod
    def _product_whole(product, price):
        return {"name": product.name,
                "disk": product.disk,
                "id": product.id,
                "bandwidth": product.bandwidth,
                "num_websites": product.num_websites,
                "num_mysqldbs": product.num_mysqldbs,
                "price": price.price, }

    @staticmethod
    @cache.memoize(3600)
    def get_products():
        results = db.session.query(HostingProduct, HostingPrice).\
            filter(HostingProduct.id == HostingPrice.product_id).\
            filter(HostingPrice.term == '1m').\
            filter(HostingProduct.deleted == 0).\
            order_by(HostingProduct.order).\
            all()
        products = [API._product_whole(product, price)
                    for product, price in results]
        return products

    def get_trial_product(self):
        product = db.session.query(HostingProduct).\
            filter(HostingProduct.deleted == 0).\
            filter(HostingProduct.num_websites != 0).\
            order_by(HostingProduct.num_websites).\
            first()

        return self._product_basic(product)
