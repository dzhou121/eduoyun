from uni.vastclient.client import Client


class API(object):

    def __init__(self, osuser_id=None):
        self.client = Client(osuser_id)

    def get(self, website_id):
        return self.client.websites.show(website_id).get('website')

    def get_all(self):
        return self.client.websites.list().get('websites')

    def get_all_apps(self, website_id):
        apps = self.client.apps.list(website_id).get('apps')
        mysqlusers = self.client.mysqlusers.list().get('mysqlusers')
        mysqldbs = self.client.mysqldbs.list().get('mysqldbs')

        for app in apps:
            for mysqluser in mysqlusers:
                if mysqluser['id'] == app['mysqluser_id']:
                    app['mysqluser_username'] = mysqluser['username']
                    app['mysqluser_password'] = mysqluser['password']

            for mysqldb in mysqldbs:
                if mysqldb['id'] == app['mysqldb_id']:
                    app['mysqldb_name'] = mysqldb['name']

        return apps

    def create(self, domain, admin_email):
        resp = self.client.websites.create(domain, admin_email)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if resp:
            if 'website' in resp:
                response = {
                    "severity": "success",
                    "message": "website is created successfully",
                }
            elif 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response

    def delete(self, website_id):
        resp = self.client.websites.delete(website_id)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if not resp:
            response = {
                "severity": "info",
                "message": "website is being deleted",
            }
        else:
            if 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response

    def display(self, website_id, path):
        resp = self.client.websites.display(website_id, path)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if resp:
            if 'website' in resp:
                response = resp['website']
            elif 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response

    def zip(self, website_id, path, entries, archname):
        resp = self.client.websites.zip(website_id, path, entries, archname)
        return self._process_response(resp, None, "Zip file created")

    def unzip(self, website_id, path, archname):
        resp = self.client.websites.unzip(website_id, path, archname)
        return self._process_response(resp, None, "File unziped")

    def delete_files(self, website_id, path, entries):
        resp = self.client.websites.delete_files(website_id, path, entries)
        return self._process_response(resp, None, "File deleted")

    def download(self, website_id, path, file):
        resp = self.client.websites.download(website_id, path, file)
        return resp['website']['content'].decode('base64')

    def upload(self, website_id, path, file, file_name):
        file = file.read().encode('base64')
        resp = self.client.websites.upload(website_id, path, file, file_name)
        return self._process_response(resp, None, "File uploaded")

    def change_ror_path(self, website_id, path):
        resp = self.client.websites.change_ror_path(website_id, path)
        return self._process_response(resp, None, "ror path changed")

    def install_gem(self, website_id):
        resp = self.client.websites.install_gem(website_id)
        return self._process_response(resp, None, "Gem installed successfully")

    def restart_ror(self, website_id):
        resp = self.client.websites.restart_ror(website_id)
        return self._process_response(resp, None, "ror restarted")

    def enable_ror(self, website_id, path):
        resp = self.client.websites.enable_ror(website_id, path)
        return self._process_response(resp, None, "ror enabled")

    def disable_ror(self, website_id):
        resp = self.client.websites.disable_ror(website_id)
        return self._process_response(resp, None, "ror disabled")

    def ror_init_db(self, website_id):
        resp = self.client.websites.ror_init_db(website_id)
        return self._process_response(resp, None, "ror db initiated")

    def ror_precompile_assets(self, website_id):
        resp = self.client.websites.ror_precompile_assets(website_id)
        return self._process_response(resp, None, "ror assets precompiled")

    def check_ror_path(self, website_id):
        resp = self.client.websites.check_ror_path(website_id)
        return resp.get('exists', False)

    def get_ror_path(self, website_id):
        resp = self.client.websites.get_ror_path(website_id)
        return resp.get('path', '')

    def check_ror_status(self, website_id):
        resp = self.client.websites.check_ror_status(website_id)
        enabled = resp.get('ror_enabled', False)
        return enabled

    def check_ror_gem(self, website_id):
        resp = self.client.websites.check_ror_gem(website_id)
        if resp is None:
            return True
        else:
            return False

    def create_app(self, name, path, website_id):
        resp = self.client.apps.create(name, path, website_id)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if resp:
            if 'app' in resp:
                response = {
                    "severity": "success",
                    "message": "app is created successfully",
                }
            elif 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response

    def delete_app(self, app_id, website_id):
        resp = self.client.apps.delete(app_id, website_id)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if not resp:
            response = {
                "severity": "info",
                "message": "app is being deleted",
            }
        else:
            if 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response

    def _process_response(self, resp, expected, success_msg):
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if expected is None:
            if not resp:
                response['severity'] = 'success'
                response['message'] = success_msg
                return response
        else:
            if expected in resp:
                response['severity'] = 'success'
                response['message'] = success_msg
                return response

        if 'badMethod' in resp:
            response['severity'] = 'error'
            response['message'] = resp['badMethod']['message']

        return response
