from uni.vastclient.client import Client


class API(object):

    def __init__(self, osuser_id=None):
        self.client = Client(osuser_id)

    def get_mysqluser(self, mysqluser_id):
        return self.client.mysqlusers.show(mysqluser_id).get('mysqluser')

    def get_mysqldb(self, mysqldb_id):
        return self.client.mysqldbs.show(mysqldb_id).get('mysqldb')

    def get_all_mysqlusers(self):
        return self.client.mysqlusers.list().get('mysqlusers')

    def get_all_mysqldbs(self):
        return self.client.mysqldbs.list().get('mysqldbs')
        
    def get_all_mysqlmemberships(self):
        return self.client.mysqlmemberships.list().get('mysqlmemberships')

    def delete_mysqlmembership(self, mysqluser_id, mysqldb_id):
        response = {"severity": "error",
                    "message": "Unknown Error", }
        mysqlmemberships = self.client.mysqlmemberships.list().\
                get('mysqlmemberships')
    
        for mysqlmembership in mysqlmemberships:
            if mysqlmembership['mysqldb_id'] == mysqldb_id \
                and mysqluser_id == mysqlmembership['mysqluser_id']:
                mysqlmembership_id = mysqlmembership['id']
                resp = self.client.mysqlmemberships.delete(mysqlmembership_id)
                if not resp:
                    response = {
                        "severity": "info",
                        "message": "mysql membership is being deleted", 
                    }
                else:
                    if 'badMethod' in resp:
                        response = {
                            "severity": "error",
                            "message": resp['badMethod']['message']
                        }
        return response
    
    def create_mysqlmemebership(self, mysqluser_id, mysqldb_id):
        response = {"severity": "error",
                    "message": "Unknown Error", }
        resp = self.client.mysqlmemberships.create(mysqluser_id,
                                              mysqldb_id)
        if resp:
            if 'mysqlmembership' in resp:
                response = {
                    "severity": "success",
                    "message": "mysql membership is created successfully", 
                }
            elif 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response
    
    def change_mysql_password(self, mysqluser_id, password):
        resp = self.client.mysqlusers.change_password(mysqluser_id, password)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if not resp:
            response = {
                "severity": "info",
                "message": "password changed successfully", 
            }
        else:
            if 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response
    
    def create_mysqluser(self, username, password):
        resp = self.client.mysqlusers.create(username, password)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if resp:
            if 'mysqluser' in resp:
                response = {
                    "severity": "success",
                    "message": "mysql user is created successfully", 
                }
            elif 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response
    
    def delete_mysqluser(self, mysqluser_id):
        resp = self.client.mysqlusers.delete(mysqluser_id)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if not resp:
            response = {
                "severity": "info",
                "message": "mysql user is being deleted", 
            }
        else:
            if 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response
    
    def create_mysqldb(self, name, charset):
        resp = self.client.mysqldbs.create(name, charset)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if resp:
            if 'mysqldb' in resp:
                response = {
                    "severity": "success",
                    "message": "mysql db is created successfully", 
                }
            elif 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response
    
    def delete_mysqldb(self, mysqldb_id):
        resp = self.client.mysqldbs.delete(mysqldb_id)
        response = {"severity": "error",
                    "message": "Unknown Error", }
        if not resp:
            response = {
                "severity": "info",
                "message": "mysql db is being deleted", 
            }
        else:
            if 'badMethod' in resp:
                response = {
                    "severity": "error",
                    "message": resp['badMethod']['message']
                }
        return response
