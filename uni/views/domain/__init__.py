# coding=utf-8

from flask import Blueprint, render_template, abort, request, jsonify
from uni import domain

bp = Blueprint('domain', __name__, url_prefix='/domain')
domain_api = domain.API()


@bp.route('/')
def index():
    tlds = domain_api.get_tlds()
    return render_template('domain/index.html',
                           tlds=tlds)


@bp.route('/search')
def search():
    domain_name = request.args.get('domain_name', None)
    tld_id = request.args.get('tld_id', None)
    #csrf = request.args.get('csrf', None)

    if not tld_id or not domain_name:
        #or csrf != request.session.get_csrf_token():
        return abort(404)

    availability = domain_api.check_availability(domain_name, tld_id)
    mappings = {'available': u'可注册',
                'regthroughothers': u'已被注册',
                'regthroughus': u'已被注册', }
    availability = mappings.get(availability, u'未知错误')

    prices = {}
    for price in domain_api.get_prices(tld_id, voucher=[]):
        prices[price['term'][:-1]] = str(price['price'])

    return jsonify(availability=availability,
                   domain_name=domain_name,
                   prices=prices)
