# -*- coding: utf-8 -*-

from flask import Blueprint, flash, request, redirect, url_for
from flask import g, render_template
from flask.ext.login import login_required, login_user, logout_user
from flask.ext.babel import gettext as _
from uni import user, invoice, login_manager
from uni.tasks import send_email
from uni.forms import SignupForm, LoginForm, PasswordChangeForm

bp = Blueprint('user', __name__, url_prefix='/user')
user_api = user.API()
invoice_api = invoice.API()


@login_manager.user_loader
def load_user(user_id):
    return user_api.load_user(user_id)


@bp.route('/')
@login_required
def index():
    username = g.user.username
    unpaid_invoices = invoice_api.get_unpaid_invoices_by_user(username)
    return render_template('user/index.html',
                           unpaid_invoices=unpaid_invoices)


@bp.route('/login', methods=["GET", "POST"])
def login():
    if g.user is not None:
        return redirect("https://%s%s" % (request.host, url_for("user.index")))

    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        remember_me = form.remember_me.data

        user = user_api.validate(username, password)
        if user:
            login_user(user_api.load_user(user['id']), remember=remember_me)
            return redirect(request.args.get("next") or "https://%s%s" %
                            (request.host, url_for("user.index")))
        else:
            flash('Failed login', 'error')

    return render_template('user/login.html',
                           form=form)


@bp.route('/signup', methods=["GET", "POST"])
def signup():
    form = SignupForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        user = user_api.get_by_username(username)
        if user:
            flash("Username already exists", "error")
        else:
            user = user_api.create(username, password)
            login_user(user_api.load_user(user['id']))
            return redirect("https://%s%s" %
                            (request.host, url_for("user.index")))

    return render_template('user/signup.html',
                           form=form)


@bp.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(request.args.get("next") or "http://%s%s" %
                    (request.host, url_for("index")))


@bp.route("/invoices/")
@login_required
def invoices():
    username = g.user.username
    invoices = invoice_api.get_invoices_by_user(username)
    return render_template("user/invoices.html",
                           invoices=invoices)


@bp.route("/invoices/<invoice_id>")
@login_required
def invoices_show(invoice_id):
    invoice_id = int(invoice_id)
    username = g.user.username
    invoice = invoice_api.validate_by_user(username, invoice_id)
    if not invoice:
        return redirect(url_for("user.invoices"))

    invoice_items = invoice_api.get_items_by_invoice_id(invoice_id)
    total_amount = 0
    for invoice_item in invoice_items:
        total_amount += invoice_item['amount']
    return render_template("user/invoices_show.html",
                           total_amount=total_amount,
                           invoice_items=invoice_items,
                           invoice=invoice)


@bp.route("/password", methods=["GET", "POST"])
@login_required
def password():
    username = g.user.username
    message = ''
    form = PasswordChangeForm()
    if form.validate_on_submit():
        original_password = request.form['original_password']
        new_password = form.password.data
        message = user_api.change_password(username,
                                           original_password,
                                           new_password)
    return render_template("user/password.html",
                           message=message,
                           form=form)


@bp.route("/pwreset/", methods=["GET", "POST"])
def pwreset():
    email = ''
    message = ''
    response = ''
    if 'submit' in request.form:
        email = request.form['email']
        user = user_api.get_by_username(email)
        if user:
            token = user_api.create_pwreset_token(user['id'])
            subject = _("password reset - yiduoyun")
            recipients = [email]
            url = "https://%s%s%s" % (request.host,
                                      url_for("user.pwreset"),
                                      token)
            body = "<p>%s,</p>" % _("Dear Customer")
            body += "<p>%s</p>" % _("You submitted a password reset request "
                                    "recently. If you did not, please ignore "
                                    "this meesage. If you do, please click "
                                    "the link below to reset your password.")
            body += '<a href="%s">%s</a>' % (url, url)
            sender = '易多云 客户支持 <support@eduoyun.com>'
            send_email.delay(sender, recipients, subject, body)
            response = ('Password reset link sent to your mailbox,'
                        ' it will be valid for the next 24 hours')
        else:
            message = 'This email does not exist'

    return render_template("user/pwreset.html",
                           message=message,
                           response=response,
                           email=email)


@bp.route("/pwreset/<token>", methods=["GET", "POST"])
def pwreset_change(token):
    user_id = user_api.validate_pwreset_token(token)
    message = ''
    if user_id is None:
        message = "url invalid"
    elif 'change_password' in request.form:
        new_password = request.form['password']
        user_api.reset_password(user_id, new_password)
        message = 'Password changed'
    return render_template("user/pwreset_change.html",
                           message=message)
