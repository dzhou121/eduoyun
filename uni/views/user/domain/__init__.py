import json
from functools import wraps
from flask import Blueprint, render_template, g, redirect, url_for, jsonify
from flask import request
from flask.ext.login import login_required
from flask.ext.babel import gettext as _

from uni import province, country
from uni.domain import API
from uni.forms import ContactForm
from uni.views.site import add_contact

domain_api = API()
PROVINCE = province.PROVINCE
COUNTRY = country.COUNTRY

bp = Blueprint('user.domain', __name__, url_prefix='/user/domain')


def domain_required(f):
    @wraps(f)
    def decorated_function(domain_id, *args, **kwargs):
        domain = domain_api.get_domain_by_id_and_username(domain_id,
                                                          g.user.username)
        if domain is None or domain['status'] != 'active':
            return redirect(url_for('user.domain.index'))
        else:
            return f(domain, *args, **kwargs)
    return decorated_function


@bp.route('/')
@login_required
def index():
    username = g.user.username
    domains = domain_api.get_domains_by_username(username)
    return render_template("user/domain/index.html",
                           domains=domains)


@bp.route('/<domain_id>')
@login_required
@domain_required
def show(domain):
    domains = domain_api.get_domains_by_username(g.user.username)
    domains_list = json.dumps(
        [{"name": "%s.%s" % (d['name'], d['tld']),
          "id": d['id'], }
         for d in domains]
    )
    return render_template("user/domain/show.html",
                           domains=domains,
                           domains_list=domains_list,
                           domain=domain)


@bp.route('/<domain_id>/dns')
@login_required
@domain_required
def dns(domain):
    dns = domain_api.get_dns_by_domain_id(domain['id'])
    return render_template("user/domain/dns.html",
                           dns=dns,
                           domain=domain)


def translate_resp(request, resp, values):
    if 'msg' in resp:
        resp['message'] = resp['msg']
    if 'message' in resp:
        message = resp['message']
        mapping = {}
        invalid = {'IP address': 'content',
                   'name': 'hostname',
                   'cname': 'cname',
                   'ttl': 'ttl',
                   'priority': 'priority', }
        for key, value in invalid.items():
            if "Invalid %s" % key in message:
                message = resp['message'].replace(values[value],
                                                  '%s(%s)s' % ('%', value))
                mapping.update({value: values[value]})
        translated_message = _(message, **mapping)
        if translated_message != message % (mapping):
            resp['message'] = translated_message
        else:
            resp['message'] = _('Unknown Error')
            print '------------------------- un-translated ----------------'
            print message
            # and should notify

    return resp


@bp.route('/<domain_id>/add_dns', methods=["POST"])
@login_required
@domain_required
def add_dns(domain):
    hostname = request.form.get('hostname', '@').lower().strip()
    type = request.form.get('type', '')
    content = request.form.get('content', '').lower().strip()
    priority = request.form.get('priority', '').lower().strip()
    ttl = request.form.get('ttl', '').lower().strip()
    values = {'hostname': hostname,
              'type': type,
              'content': content,
              'priority': priority,
              'ttl': ttl, }

    resp = domain_api.add_dns(domain['id'], hostname, type,
                              content, priority, ttl)
    return jsonify(translate_resp(request, resp, values))


@bp.route('/<domain_id>/modify_dns', methods=["POST"])
@login_required
@domain_required
def modify_dns(domain):
    hostname = request.form.get('hostname', '@').lower().strip()
    type = request.form.get('type', '')
    content = request.form.get('content', '').lower().strip()
    priority = request.form.get('priority', '').lower().strip()
    ttl = request.form.get('ttl', '').lower().strip()
    record_id = request.form.get('record_id')
    values = {'hostname': hostname,
              'type': type,
              'content': content,
              'priority': priority,
              'ttl': ttl, }

    resp = domain_api.modify_dns(domain['id'], record_id,
                                 hostname, type, content, priority, ttl)
    return jsonify(translate_resp(request, resp, values))


@bp.route('/<domain_id>/delete_dns', methods=["POST"])
@login_required
@domain_required
def delete_dns(domain):
    record_id = request.form.get('record_id')
    resp = domain_api.delete_dns(domain['id'], record_id)
    return jsonify(translate_resp(request, resp, {}))


@bp.route('/<domain_id>/ns', methods=["GET", "POST"])
@login_required
@domain_required
def ns(domain):
    default_ns = domain_api.get_default_ns()
    resp = None
    if 'update-ns' in request.form:
        new_ns = request.form.getlist('ns')
        new_ns = [ns.lower().strip() for ns in new_ns if ns.lower().strip()]
        new_ns.sort()
        resp = domain_api.update_ns(domain['id'], new_ns)
    if 'reset-ns' in request.form:
        resp = domain_api.update_ns(domain['id'], default_ns)

    domain = domain_api.get_domain_by_id(domain['id'])
    ns = json.loads(domain['ns'])
    glue = domain_api.get_glue(domain['id'])
    return render_template("user/domain/ns.html",
                           ns=ns,
                           default_ns=default_ns,
                           resp=resp,
                           glue=glue,
                           domain=domain)


@bp.route('/<domain_id>/add_glue', methods=["POST"])
@login_required
@domain_required
def add_glue(domain):
    hostname = request.form.get('hostname', '').lower().strip()
    ip = request.form.get('ip', '').lower().strip()
    values = {'hostname': hostname,
              'ip': ip, }

    resp = domain_api.add_glue(domain['id'], hostname, ip)
    return jsonify(translate_resp(request, resp, values))


@bp.route('/<domain_id>/modify_glue', methods=["POST"])
@login_required
@domain_required
def modify_glue(domain):
    hostname = request.form.get('hostname', '').lower().strip()
    ip = request.form.get('ip', '').lower().strip()
    glue_id = request.form.get('glue_id')
    values = {'hostname': hostname,
              'type': type, }
    resp = domain_api.modify_glue(domain['id'], glue_id, hostname, ip)
    return jsonify(translate_resp(request, resp, values))


@bp.route('/<domain_id>/delete_glue', methods=["POST"])
@login_required
@domain_required
def delete_glue(domain):
    glue_id = request.form.get('glue_id')
    resp = domain_api.delete_glue(domain['id'], glue_id)
    return translate_resp(request, resp, {})


@bp.route('/<domain_id>/contact', methods=["GET", "POST"])
@login_required
@domain_required
def contact(domain):
    message = None
    form = ContactForm()
    create_contact = False
    if 'confirm-create-contact' in request.form:
        if not form.validate_on_submit():
            create_contact = True
        else:
            resp = add_contact(g.user.id, form,
                               create_resellbiz=True)
            if resp['status'] == 'failed':
                msg = _(resp['message'])
                if msg == resp['message']:
                    print resp['message']
                    msg = _('Contact created failed')
                    # should notify
                message = {'severity': 'error',
                           'msg': msg, }
            else:
                message = {
                    'severity': 'success',
                    'msg': _('Contact created successfully'),
                }

    if 'change-contact' in request.form:
        reg_contact = request.form.get('reg-contact')
        admin_contact = request.form.get('admin-contact')
        tech_contact = request.form.get('tech-contact')
        billing_contact = request.form.get('billing-contact')
        resp = domain_api.change_contact(domain['id'], reg_contact,
                                         admin_contact,
                                         tech_contact, billing_contact)
        if resp['status'] != 'success':
            msg = _(resp['message'])
            if msg == resp['message']:
                msg = _('Contact changed failed')
            message = {'severity': 'error',
                       'msg': msg, }
        else:
            message = {
                'severity': 'success',
                'msg': _('Contact changed successfully'),
            }

    domain = domain_api.get_domain_by_id(domain['id'])
    contacts = domain_api.get_contacts_by_username(g.user.username)
    return render_template("user/domain/contact.html",
                           contacts=contacts,
                           message=message,
                           form=form,
                           create_contact=create_contact,
                           city=request.form.get('city'),
                           province=request.form.get('province'),
                           country=request.form.get('country'),
                           PROVINCE=PROVINCE,
                           COUNTRY=COUNTRY,
                           domain=domain)
