import StringIO
import urlparse
from functools import wraps
from flask import Blueprint, render_template, g, redirect, url_for, flash
from flask import request, send_file, session, jsonify
from flask.ext.login import login_required
from werkzeug import secure_filename
from uni.forms import HostingWebsiteForm
from uni.hosting import API as HostingAPI
from uni.hosting.website import API as WebsiteAPI
from uni.hosting.ftpuser import API as FtpuserAPI
from uni.hosting.mysql import API as MysqlAPI

hosting_api = HostingAPI()

bp = Blueprint('user.hosting', __name__, url_prefix='/user/hosting')


def hosting_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        hosting = hosting_api.get_current(g.user.id)
        if hosting is None or hosting['status'] != 'active':
            return redirect(url_for('user.hosting.index'))
        else:
            return f(hosting, *args, **kwargs)
    return decorated_function


@bp.route('/', methods=["GET", "POST"])
@login_required
def index():
    user_id = g.user.id

    if 'hosting' in request.form:
        hosting_id = int(request.form['hosting'])
        session['current_hosting_id'] = hosting_id

    hostings = hosting_api.get_all(user_id)
    hosting = hosting_api.get_current(user_id)
    usage = None
    if hosting is not None:
        usage = hosting_api.get_usage(hosting)
        ssh_info = hosting_api.get_ssh_info(hosting)

    return render_template('user/hosting/index.html',
                           hostings=hostings,
                           hosting=hosting,
                           usage=usage,
                           ssh_info=ssh_info)


def check_domainname(domain):
    if not(domain.startswith('http://') or domain.startswith('https://')):
        domain = 'http://%s' % domain
    domain = urlparse.urlparse(domain).hostname
    if domain is None:
        domain = ''
    if domain.startswith('www.'):
        domain = domain[4:]
    return domain


@bp.route('/websites/', methods=["GET", "POST"])
@login_required
@hosting_required
def websites(hosting):
    website_api = WebsiteAPI(hosting['osuser_id'])
    form = HostingWebsiteForm()
    response = None
    if request.method == "POST":
        if 'delete' in request.form:
            website_id = request.form['delete']
            response = website_api.delete(website_id)
        elif 'delete_selected' in request.form:
            website_ids = request.form.getlist('website_ids')
            for website_id in website_ids:
                response = website_api.delete(website_id)
        elif form.validate_on_submit():
            domain = check_domainname(form.domain.data).encode('idna')
            if domain == '':
                response = {
                    "severity": "error",
                    "message": "Wrong domain format entered",
                }
            else:
                usage = hosting_api.get_usage(hosting)
                if usage['num_websites'] < hosting['num_websites'] or \
                   hosting['num_websites'] == 0:
                    response = website_api.create(domain, g.user.username)
                else:
                    response = {
                        "severity": "error",
                        "message": "You have reached your website quota",
                    }

    websites = website_api.get_all()
    if response is not None:
        flash(response['message'], response['severity'])

    return render_template('user/hosting/websites.html',
                           hosting=hosting,
                           form=form,
                           websites=websites)


@bp.route('/websites/<id>/', methods=["GET", "POST"])
@login_required
@hosting_required
def websites_show(hosting, id):
    website_api = WebsiteAPI(hosting['osuser_id'])
    website = website_api.get(id)
    if not website:
        return redirect(url_for('user.hosting.websites'))

    apptypes = hosting_api.get_app_types()
    response = None
    if request.method == "POST":
        if 'create_app' in request.form:
            usage = hosting_api.get_usage(hosting)
            if usage['num_mysqldbs'] < hosting['num_mysqldbs'] or \
               hosting['num_mysqldbs'] == 0:
                name = request.form['app_name']
                path = request.form['app_path']
                response = website_api.create_app(name, path, id)
            else:
                response = {
                    "severity": "error",
                    "message": "You have reached your mysqldb quota",
                }
        elif 'delete_app' in request.form:
            app_id = int(request.form['delete_app'])
            response = website_api.delete_app(app_id, id)
        elif 'delete_selected' in request.form:
            app_ids = request.form.getlist('app_ids')
            for app_id in app_ids:
                response = website_api.delete_app(app_id, id)
        elif 'change_ror_path' in request.form:
            ror_path = request.form['ror_path']
            response = website_api.change_ror_path(website['id'], ror_path)
        elif 'restart_app' in request.form:
            response = website_api.restart_ror(website['id'])
        elif 'init_db' in request.form:
            response = website_api.ror_init_db(website['id'])
        elif 'precompile_assets' in request.form:
            response = website_api.ror_precompile_assets(website['id'])
        elif 'toggle_ror' in request.form:
            if 'ror_checkbox' in request.form:
                response = website_api.enable_ror(website['id'], 'public')
            else:
                response = website_api.disable_ror(website['id'])

    if response is not None:
        flash(response['message'], response['severity'])
    apps = website_api.get_all_apps(id)

    ror_enabled = website_api.check_ror_status(website['id'])
    ror_status = {'enabled': ror_enabled}
    if ror_enabled:
        ror_path = website_api.get_ror_path(website['id'])
        ror_status.update({'path': ror_path})
        ror_path_exists = website_api.check_ror_path(website['id'])
        ror_status.update({'path_exists': ror_path_exists})
        if ror_path_exists:
            ror_gem_installed = website_api.check_ror_gem(website['id'])
            ror_status.update({'gem_installed': ror_gem_installed})

    return render_template('user/hosting/websites_show.html',
                           hosting=hosting,
                           apptypes=apptypes,
                           apps=apps,
                           ror_status=ror_status,
                           website=website)


@bp.route('/websites/<id>/install_gem', methods=["GET", "POST"])
@login_required
@hosting_required
def install_gem(hosting, id):
    website_api = WebsiteAPI(hosting['osuser_id'])
    website = website_api.get(id)
    if not website:
        return redirect(url_for('user.hosting.websites'))

    resp = website_api.install_gem(website['id'])
    return jsonify(resp=resp)


@bp.route('/websites/<id>/files/', methods=["GET", "POST"])
@bp.route('/websites/<id>/files/<path:path>', methods=["GET", "POST"])
@login_required
@hosting_required
def websites_files(hosting, id, path=''):
    website_api = WebsiteAPI(hosting['osuser_id'])
    website = website_api.get(id)
    if not website:
        return redirect(url_for('user.hosting.websites'))

    response = None
    if request.method == "POST":
        if 'zip' in request.form:
            entries = request.form.getlist('entries')
            archname = "%s.zip" % request.form.get('archname')
            response = website_api.zip(id, path, entries, archname)
        elif 'unzip' in request.form:
            archname = request.form.get('unzip')
            response = website_api.unzip(id, path, archname)
        elif 'delete' in request.form:
            entries = [request.form.get('delete')]
            response = website_api.delete_files(id, path, entries)
        elif 'delete_selected' in request.form:
            entries = request.form.getlist('entries')
            response = website_api.delete_files(id, path, entries)
        elif 'download' in request.form:
            file_name = request.form.get('download')
            file_content = website_api.download(id, path, file_name)
            return send_file(StringIO.StringIO(file_content),
                             attachment_filename=file_name,
                             add_etags=False,
                             as_attachment=True)
        elif 'upload' in request.form:
            file = request.files['file']
            file_name = secure_filename(file.filename)
            response = website_api.upload(id, path, file.stream, file_name)
    if response is not None:
        flash(response['message'], response['severity'])

    display = website_api.display(id, path)

    url = request.url
    tree = [{"link": "/user/hosting/websites/%s/files/" % id,
             "entry": website['name']}]
    append = lambda x: "%s%s/" % (tree[-1]['link'], x)

    if path.endswith('/'):
        path = path[:-1]

    if path == '':
        path_list = []
    else:
        path_list = path.split('/')
    for path in path_list:
        tree.append({"link": append(path), "entry": path})

    return render_template('user/hosting/websites_files.html',
                           display=display,
                           url=url,
                           tree=tree,
                           hosting=hosting,
                           website=website)


@bp.route('/ftpusers/', methods=["GET", "POST"])
@login_required
@hosting_required
def ftpusers(hosting):
    ftpuser_api = FtpuserAPI(hosting['osuser_id'])

    response = None
    if request.method == "POST":
        if 'create' in request.form:
            ftpusername = request.form['ftpusername']
            ftppassword = request.form['ftppassword']
            if 'account_type' in request.form and\
                    request.form['account_type'] == 'admin':
                admin = 1
            else:
                admin = 0
            response = ftpuser_api.create_ftpuser(ftpusername,
                                                  ftppassword,
                                                  admin)
        elif 'delete' in request.form:
            ftpuser_id = request.form['delete']
            response = ftpuser_api.delete_ftpuser(ftpuser_id)
        elif 'delete_selected' in request.form:
            ftpuser_ids = request.form.getlist('ftpuser_ids')
            for ftpuser_id in ftpuser_ids:
                response = ftpuser_api.delete_ftpuser(ftpuser_id)

    ftpusers = ftpuser_api.get_all()
    if response is not None:
        flash(response['message'], response['severity'])
    return render_template('user/hosting/ftpusers.html',
                           ftpusers=ftpusers,
                           hosting=hosting)


@bp.route('/ftpusers/<ftpuser_id>', methods=["GET", "POST"])
@login_required
@hosting_required
def ftpusers_show(hosting, ftpuser_id):
    ftpuser_api = FtpuserAPI(hosting['osuser_id'])
    website_api = WebsiteAPI(hosting['osuser_id'])
    ftpuser = ftpuser_api.get(ftpuser_id)
    if not ftpuser:
        return redirect(url_for('user.hosting.ftpusers'))
    websites = website_api.get_all()

    response = None
    if request.method == "POST":
        if 'create_membership' in request.form:
            website_id = int(request.form['create_membership'])
            ftpuser_id = int(ftpuser_id)
            response = ftpuser_api.create_websitemembership(ftpuser_id,
                                                            website_id)
        elif 'delete_membership' in request.form:
            website_id = int(request.form['delete_membership'])
            ftpuser_id = int(ftpuser_id)
            response = ftpuser_api.delete_websitemembership(ftpuser_id,
                                                            website_id)
        elif 'add_selected' in request.form:
            ftpuser_id = int(ftpuser_id)
            website_ids = request.form.getlist('website_ids')
            for website_id in website_ids:
                website_id = int(website_id)
                response = ftpuser_api.create_websitemembership(ftpuser_id,
                                                                website_id)
        elif 'delete_selected' in request.form:
            ftpuser_id = int(ftpuser_id)
            website_ids = request.form.getlist('website_ids')
            for website_id in website_ids:
                website_id = int(website_id)
                response = ftpuser_api.delete_websitemembership(ftpuser_id,
                                                                website_id)
        elif 'change_password' in request.form:
            password = request.form['ftppassword']
            response = ftpuser_api.change_password(ftpuser_id,
                                                   password)

    websitememberships = ftpuser_api.get_all_websitememberships()
    active_websites = [website for website in websites
                       if website['status'] == 'active']
    managed_websites_ids = [websitemembership['website_id']
                            for websitemembership in websitememberships
                            if websitemembership['ftpuser_id'] ==
                            int(ftpuser_id)]
    managed_websites = [website for website in active_websites
                        if website['id'] in managed_websites_ids]
    unmanaged_websites = [website for website in active_websites
                          if website['id'] not in managed_websites_ids]
    if response is not None:
        flash(response['message'], response['severity'])
    return render_template('user/hosting/ftpusers_show.html',
                           ftpuser=ftpuser,
                           websitememberships=websitememberships,
                           managed_websites=managed_websites,
                           unmanaged_websites=unmanaged_websites,
                           hosting=hosting)


@bp.route('/mysqlusers/', methods=["GET", "POST"])
@login_required
@hosting_required
def mysqlusers(hosting):
    mysql_api = MysqlAPI(hosting['osuser_id'])
    response = None
    if request.method == "POST":
        if 'create_mysqluser' in request.form:
            mysql_username = request.form['mysqlusername']
            mysql_password = request.form['mysqluserpassword']
            response = mysql_api.create_mysqluser(mysql_username,
                                                  mysql_password)
        elif 'delete_mysqluser' in request.form:
            mysqluser_id = request.form['delete_mysqluser']
            response = mysql_api.delete_mysqluser(mysqluser_id)
        elif 'delete_selected' in request.form:
            mysqluser_ids = request.form.getlist('mysqluser_ids')
            for mysqluser_id in mysqluser_ids:
                response = mysql_api.delete_mysqluser(mysqluser_id)
    mysqlusers = mysql_api.get_all_mysqlusers()
    if response is not None:
        flash(response['message'], response['severity'])
    return render_template('user/hosting/mysqlusers.html',
                           mysqlusers=mysqlusers,
                           hosting=hosting)


@bp.route('/mysqlusers/<mysqluser_id>', methods=["GET", "POST"])
@login_required
@hosting_required
def mysqlusers_show(hosting, mysqluser_id):
    mysql_api = MysqlAPI(hosting['osuser_id'])
    mysqluser = mysql_api.get_mysqluser(mysqluser_id)
    if not mysqluser:
        return redirect(url_for('user.hosting.mysqlusers'))

    response = None
    if request.method == "POST":
        if 'create_membership' in request.form:
            added_mysqldb_id = int(request.form['create_membership'])
            added_mysqluser_id = int(mysqluser_id)
            response = mysql_api.create_mysqlmemebership(added_mysqluser_id,
                                                         added_mysqldb_id)
        elif 'delete_membership' in request.form:
            removed_mysqldb_id = int(request.form['delete_membership'])
            removed_mysqluser_id = int(mysqluser_id)
            response = mysql_api.delete_mysqlmembership(removed_mysqluser_id,
                                                        removed_mysqldb_id)
        elif 'add_selected' in request.form:
            added_mysqluser_id = int(mysqluser_id)
            mysqldb_ids = request.form.getlist('mysqldb_ids')
            for mysqldb_id in mysqldb_ids:
                added_mysqldb_id = int(mysqldb_id)
                response = mysql_api.create_mysqlmemebership(
                    added_mysqluser_id,
                    added_mysqldb_id)
        elif 'delete_selected' in request.form:
            removed_mysqluser_id = int(mysqluser_id)
            mysqldb_ids = request.form.getlist('mysqldb_ids')
            mysqlmemberships = mysql_api.get_all_mysqlmemberships()
            for mysqldb_id in mysqldb_ids:
                removed_mysqldb_id = int(mysqldb_id)
                response = mysql_api.delete_mysqlmembership(
                    removed_mysqluser_id,
                    removed_mysqldb_id)
        elif 'change_password' in request.form:
            password = request.form['mysqluserpassword']
            response = mysql_api.change_mysql_password(mysqluser_id,
                                                       password)

    mysqlmemberships = mysql_api.get_all_mysqlmemberships()
    mysqldbs = mysql_api.get_all_mysqldbs()
    active_mysqldbs = [mysqldb for mysqldb in mysqldbs
                       if mysqldb['status'] == 'active']
    managed_mysqldbs_ids = [mysqlmembership['mysqldb_id']
                            for mysqlmembership in mysqlmemberships
                            if mysqlmembership['mysqluser_id'] ==
                            int(mysqluser_id)]
    managed_mysqldbs = [mysqldb for mysqldb in active_mysqldbs
                        if mysqldb['id'] in managed_mysqldbs_ids]
    unmanaged_mysqldbs = [mysqldb for mysqldb in active_mysqldbs
                          if mysqldb['id'] not in managed_mysqldbs_ids]
    if response is not None:
        flash(response['message'], response['severity'])
    return render_template('user/hosting/mysqlusers_show.html',
                           managed_mysqldbs=managed_mysqldbs,
                           mysqluser=mysqluser,
                           mysqlmemberships=mysqlmemberships,
                           unmanaged_mysqldbs=unmanaged_mysqldbs,
                           hosting=hosting)


@bp.route('/mysqldbs/', methods=["GET", "POST"])
@login_required
@hosting_required
def mysqldbs(hosting):
    mysql_api = MysqlAPI(hosting['osuser_id'])
    response = None
    if request.method == "POST":
        if 'create_mysqldb' in request.form:
            usage = hosting_api.get_usage(hosting)
            if usage['num_mysqldbs'] < hosting['num_mysqldbs'] or \
               hosting['num_mysqldbs'] == 0:
                mysqldb_name = request.form['mysqldbname']
                charset = request.form['charset']
                response = mysql_api.create_mysqldb(mysqldb_name, charset)
            else:
                response = {
                    "severity": "error",
                    "message": "You have reached your mysqldb quota",
                }
        elif 'delete_mysqldb' in request.form:
            mysqldb_id = request.form['delete_mysqldb']
            response = mysql_api.delete_mysqldb(mysqldb_id)
        elif 'delete_selected' in request.form:
            mysqldb_ids = request.form.getlist('mysqldb_ids')
            for mysqldb_id in mysqldb_ids:
                response = mysql_api.delete_mysqldb(mysqldb_id)
    mysqldbs = mysql_api.get_all_mysqldbs()
    if response is not None:
        flash(response['message'], response['severity'])
    return render_template('user/hosting/mysqldbs.html',
                           mysqldbs=mysqldbs,
                           hosting=hosting)


@bp.route('/mysqldbs/<mysqldb_id>', methods=["GET", "POST"])
@login_required
@hosting_required
def mysqldbs_show(hosting, mysqldb_id):
    mysql_api = MysqlAPI(hosting['osuser_id'])
    mysqldb = mysql_api.get_mysqldb(mysqldb_id)
    if not mysqldb:
        return redirect(url_for('user.hosting.mysqldbs'))

    response = None
    if request.method == "POST":
        if 'create_membership' in request.form:
            added_mysqluser_id = int(request.form['create_membership'])
            added_mysqldb_id = int(mysqldb_id)
            response = mysql_api.create_mysqlmemebership(added_mysqluser_id,
                                                         added_mysqldb_id)
        elif 'delete_membership' in request.form:
            removed_mysqluser_id = int(request.form['delete_membership'])
            removed_mysqldb_id = int(mysqldb_id)
            response = mysql_api.delete_mysqlmembership(removed_mysqluser_id,
                                                        removed_mysqldb_id)
        elif 'add_selected' in request.form:
            added_mysqldb_id = int(mysqldb_id)
            mysqluser_ids = request.form.getlist('mysqluser_ids')
            for mysqluser_id in mysqluser_ids:
                added_mysqluser_id = int(mysqluser_id)
                response = mysql_api.create_mysqlmemebership(
                    added_mysqluser_id,
                    added_mysqldb_id)
        elif 'delete_selected' in request.form:
            removed_mysqldb_id = int(mysqldb_id)
            mysqluser_ids = request.form.getlist('mysqluser_ids')
            mysqlmemberships = mysql_api.get_all_mysqlmemberships()
            for mysqluser_id in mysqluser_ids:
                removed_mysqluser_id = int(mysqluser_id)
                response = mysql_api.delete_mysqlmembership(
                    removed_mysqluser_id,
                    removed_mysqldb_id)
    mysqlmemberships = mysql_api.get_all_mysqlmemberships()
    mysqlusers = mysql_api.get_all_mysqlusers()
    active_mysqlusers = [mysqluser for mysqluser in mysqlusers
                         if mysqluser['status'] == 'active']
    managed_mysqlusers_ids = [mysqlmembership['mysqluser_id']
                              for mysqlmembership in mysqlmemberships
                              if mysqlmembership['mysqldb_id'] ==
                              int(mysqldb_id)]
    managed_mysqlusers = [mysqluser for mysqluser in active_mysqlusers
                          if mysqluser['id'] in managed_mysqlusers_ids]
    unmanaged_mysqlusers = [mysqluser for mysqluser in active_mysqlusers
                            if mysqluser['id'] not in managed_mysqlusers_ids]
    if response is not None:
        flash(response['message'], response['severity'])
    return render_template('user/hosting/mysqldbs_show.html',
                           mysqldb=mysqldb,
                           mysqlmemberships=mysqlmemberships,
                           managed_mysqlusers=managed_mysqlusers,
                           unmanaged_mysqlusers=unmanaged_mysqlusers,
                           hosting=hosting)
