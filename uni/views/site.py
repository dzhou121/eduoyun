import json
import urllib
from flask import render_template, request, session, redirect, g, make_response
from flask import url_for, Response
from flask.ext.login import login_user, current_user
from uni import hosting, domain, user, province, country, voucher, invoice
from uni import app, db
from uni.views.user import login_required
from uni.alipay import Alipay
from uni.forms import SignupForm, ContactForm

hosting_api = hosting.API()
domain_api = domain.API()
user_api = user.API()
voucher_api = voucher.API()
invoice_api = invoice.API()


@app.before_request
def load_current_user():
    if current_user.is_authenticated():
        g.user = current_user
        if 'login_as' in session:
            login_as = session['login_as']
            user_id = login_as['user_id']
            g.user.id = user_id
            g.user.user_id = user_id
            g.user.username = login_as['username']
    else:
        g.user = None


@app.teardown_request
def commit_db_session(exception=None):
    try:
        db.session.commit()
    except:
        db.session.rollback()


@app.errorhandler(404)
def not_found(error):
    return render_template('site/404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template('site/500.html'), 500


@app.route('/')
def index():
    products = hosting_api.get_products()
    return render_template('site/index.html',
                           products=products)


@app.route('/faq')
def faq():
    return render_template('site/faq.html')


@app.route('/contact')
def contact():
    return render_template('site/contact.html')


PROVINCE = province.PROVINCE
COUNTRY = country.COUNTRY


def products_in_cart(request, voucher):
    products = []
    if voucher:
        voucher = voucher['description']
    else:
        voucher = []
    if 'cart' in request.cookies:
        cart = request.cookies.get('cart')
        cart_items = json.loads(urllib.unquote(cart))['cart']

        for cart_item in cart_items:
            product_id = cart_item['product_id']
            category = cart_item['category']
            term = cart_item.get('term')
            type = cart_item['type']
            item_id = cart_item.get('item_id')
            if category == 'domain':
                prices = domain_api.get_prices(product_id, voucher)
                for price in prices:
                    if price['term'] == term:
                        current_price = price['price']
                        current_discount = price['discount']
                        break
            elif cart_item.get('category') == 'hosting':
                prices = hosting_api.get_prices_by_name(cart_item.get('name'),
                                                        voucher,
                                                        type,
                                                        item_id)
                current_price = prices[0]['price']
                current_discount = prices[0]['discount']
                term = cart_item.get('term')
                category = cart_item.get('category')
                if not term:
                    term = prices[0]['term']
                else:
                    for price in prices:
                        if price['term'] == term:
                            current_price = price['price']
                            current_discount = price['discount']
            else:
                continue
            products.append({"name": cart_item.get('name'),
                             "type": cart_item.get('type'),
                             "item_id": item_id,
                             "term": term,
                             "product_id": product_id,
                             "category": category,
                             "auth_code": cart_item.get('auth_code'),
                             "current_price": current_price,
                             "current_discount": current_discount,
                             "prices": prices, })
    return products


def add_invoice(request, user_id, products, voucher, contact):
    voucher_id = None
    if voucher:
        voucher_id = voucher['id']
        voucher_api.increment_uses(voucher['name'])
        cancel_voucher(request)

    invoice = invoice_api.create(user_id, voucher_id)
    add_invoice_items(user_id, invoice['id'], products, contact)
    return invoice


def add_invoice_items(user_id, invoice_id, products, contact):
    for product in products:
        term = product['term']
        type = product['type']
        product_id = product['product_id']
        item_id = product['item_id']
        category = product['category']
        price = product['current_price']
        discount = product['current_discount']

        if category == 'hosting':
            if type == 'purchase':
                item = hosting_api.create(user_id, product_id)
                item_id = item['id']
        elif category == 'domain':
            if type == 'purchase' or type == 'transfer_in':
                domain_name = product['name'].split('.')[0]
                if type == 'purchase':
                    auth_code = None
                elif type == 'transfer_in':
                    auth_code = product['auth_code']
                item = domain_api.create(user_id, domain_name, product_id,
                                         contact['id'], contact['id'],
                                         contact['id'], contact['id'],
                                         auth_code=auth_code)
                item_id = item['id']

        invoice_api.create_item(invoice_id, item_id, product_id, type,
                                price, discount, term, category)


def get_voucher(request):
    if 'voucher' in session:
        return voucher_api.get_by_name(session['voucher'])
    return {}


def update_voucher(request):
    voucher_name = request.form['voucher']
    voucher = voucher_api.get_by_name(voucher_name)
    if voucher:
        session['voucher'] = voucher['name']
    else:
        return "No such voucher"


def cancel_voucher(request):
    if 'voucher' in session:
        del(session['voucher'])


def check_contact_required(products):
    for product in products:
        if product['category'] == 'domain' and product['type'] != 'renew':
            return True

    return False


def add_contact(user_id, contact_form, create_resellbiz):
    company = contact_form.company.data or 'N/A'
    return domain_api.create_contact(user_id,
                                     contact_form.name.data,
                                     contact_form.contact_email.data,
                                     company,
                                     contact_form.telephone.data,
                                     contact_form.address.data,
                                     request.form.get('city'),
                                     request.form.get('province'),
                                     request.form.get('country'),
                                     contact_form.postcode.data,
                                     create_resellbiz=create_resellbiz)


@app.route('/cart', methods=["GET", "POST"])
def cart():
    signup_form = SignupForm()
    contact_form = ContactForm()
    response = None
    if "update_voucher" in request.form:
        response = update_voucher(request)

    if "cancel_voucher" in request.form:
        cancel_voucher(request)

    voucher = get_voucher(request)
    products = products_in_cart(request, voucher)
    contact_required = check_contact_required(products)
    if contact_required and g.user is not None:
        contacts = domain_api.get_contacts_by_username(g.user.username)
    else:
        contacts = []

    message = ''
    username = ''
    password = ''
    new_contact = False
    if 'new_contact' in request.form:
        new_contact = True

    if "checkout" in request.form:
        signup_form.validate_on_submit()
        contact_form.validate_on_submit()
        user_id = None
        user = g.user
        if user is not None:
            user_id = user.id
        else:
            if signup_form.validate_on_submit():
                username = signup_form.username.data
                password = signup_form.password.data
                if user_api.get_by_username(username):
                    message = "Username already exists"
                else:
                    user = user_api.create(username, password)
                    user_id = user['id']
                    user = user_api.load_user(user_id)
                    login_user(user)
                    g.user = user

        if user_id is not None:
            contact = None
            if contact_required and 'contact_submitted' in request.form:
                if new_contact:
                    if contact_form.validate_on_submit():
                        contact = add_contact(user_id, contact_form,
                                              create_resellbiz=False)
                else:
                    contact = {'id': request.form['choose_contact']}

            if (not contact_required) or (contact is not None and
                                          signup_form.accept_tos.validate(
                                              signup_form)):

                invoice = add_invoice(request, user_id, products, voucher,
                                      contact)
                resp = make_response(redirect('/invoice/%s' % invoice['id']))
                resp.set_cookie('cart', expires=0)
                return resp

    return render_template('site/cart.html',
                           products=products,
                           message=message,
                           voucher=voucher,
                           signup_form=signup_form,
                           contact_form=contact_form,
                           contact_required=contact_required,
                           contacts=contacts,
                           PROVINCE=PROVINCE,
                           COUNTRY=COUNTRY,
                           new_contact=new_contact,
                           city=request.form.get('city'),
                           province=request.form.get('province'),
                           country=request.form.get('country'),
                           response=response)


def fulfill_invoice(invoice):
    invoice_api.mark_as_paid(invoice['id'])
    invoice_items = invoice_api.get_items_by_invoice_id(invoice['id'])
    for invoice_item in invoice_items:
        type = invoice_item['type']
        category = invoice_item['category']
        item_id = invoice_item['item_id']
        term = invoice_item['term']
        product_id = invoice_item['product_id']

        if category == 'hosting':
            if type == 'purchase':
                hosting_api.active(item_id, term)
            elif type == 'renew':
                hosting_api.renew(item_id, term)
            elif type == 'upgrade':
                hosting_api.upgrade(item_id, term, product_id)
        elif category == 'domain':
            if type == 'purchase':
                domain_api.register(item_id, term)
            elif type == 'renew':
                domain_api.renew(item_id, term)
            elif type == 'transfer_in':
                domain_api.transfer_in(item_id, term)


@app.route('/invoice/<invoice_id>', methods=["GET", "POST"])
@login_required
def invoice(invoice_id):
    username = g.user.username
    invoice = invoice_api.validate_by_user(username, invoice_id)
    if not invoice:
        return redirect(url_for('index'))
    if invoice['paid']:
        return redirect(url_for('user.invoices'))

    items = invoice_api.get_items_by_invoice_id(invoice_id)

    total_amount = 0
    for invoice_item in items:
        total_amount += invoice_item['amount']

    if 'pay' in request.form:
        if total_amount > 0:
            alipay = Alipay()
            request_url = alipay.create_url(invoice['uuid'],
                                            total_amount)
            return redirect(request_url)
        else:
            fulfill_invoice(invoice)
            return redirect(url_for('user.invoices'))

    return render_template('site/invoice.html',
                           invoice_id=invoice_id,
                           items=items,
                           total_amount=total_amount)


@app.route('/alipay', methods=["GET", "POST"])
def alipay():
    alipay = Alipay()
    valid, params = alipay.validate(request)
    if valid:
        if params['trade_status'] == 'WAIT_SELLER_SEND_GOODS' \
           or params['trade_status'] == 'TRADE_FINISHED':
            out_trade_no = params['out_trade_no']
            invoice = invoice_api.get_by_uuid(out_trade_no)
            if not invoice['paid']:
                fulfill_invoice(invoice)

            if params['trade_status'] == 'WAIT_SELLER_SEND_GOODS':
                alipay.confirm_goods(params['trade_no'])

        return Response('success')
    else:
        return Response('fail')


@app.route('/city')
def city():
    province = request.args.get('province')
    for item in PROVINCE:
        if item['cn'] == province:
            return Response(json.dumps(item['city']),
                            mimetype='application/json')


@app.route('/province')
def province():
    return Response(
        json.dumps([
            {'en': item['en'], 'cn': item['cn']}
            for item in PROVINCE]
        ),
        mimetype='application/json'
    )


@app.route('/country')
def country():
    return Response(json.dumps(COUNTRY), mimetype='application/json')


@app.route('/tos')
def tos():
    return render_template('site/tos.html')


@app.route('/sitemap.xml', methods=["GET"])
def sitemap():
    pages = []
    url_root = request.url_root[:-1]

    # static pages
    for rule in app.url_map.iter_rules():
        if "GET" in rule.methods and len(rule.arguments) == 0:
            pages.append(rule.rule)

    sitemap_xml = render_template('site/sitemap.xml', pages=pages,
                                  url_root=url_root)
    response = make_response(sitemap_xml)
    response.headers["Content-Type"] = "application/xml"

    return response
