from functools import wraps
from flask import Blueprint, render_template, g, abort, request, session
from flask import redirect, url_for
from uni.domain import API as DomainAPI
from uni.invoice import API as InvoiceAPI
from uni.user import API as UserAPI
from uni.hosting import API as HostingAPI

bp = Blueprint('admin', __name__, url_prefix='/admin')
hosting_api = HostingAPI()
domain_api = DomainAPI()
invoice_api = InvoiceAPI()
user_api = UserAPI()


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        user = g.user
        if user is None or user.group != 'admin':
            abort(404)
        else:
            return f(*args, **kwargs)
    return decorated_function


@bp.route('/')
@admin_required
def index():
    pending_domains = domain_api.get_pending()
    monthly_sales = invoice_api.get_monthly_sales()
    total_sales = sum([monthly_sale['amount']
                       for monthly_sale in monthly_sales])
    return render_template('admin/index.html',
                           monthly_sales=monthly_sales,
                           pending_domains=pending_domains,
                           total_sales=total_sales)


@bp.route('/users/', methods=["GET", "POST"])
@admin_required
def users():
    searched_users = None
    if 'search' in request.form:
        username = request.form['username']
        searched_users = user_api.search_user(username)

    if 'login_as_id' in request.args:
        login_as_id = long(request.args['login_as_id'])
        login_as_username = request.args['login_as_username']
        session['login_as'] = {
            'user_id': login_as_id,
            'username': login_as_username,
        }
        return redirect(url_for('user.index'))
    return render_template('admin/users.html',
                           searched_users=searched_users)


@bp.route('/users/<id>/', methods=["GET", "POST"])
@admin_required
def users_show(id):
    user_id = long(id)
    if 'delete-hosting' in request.form:
        hosting_id = long(request.form['delete-hosting'])
        hosting_api.delete(user_id, hosting_id)
    hostings = hosting_api.get_all(user_id)
    return render_template('admin/users_show.html',
                           hostings=hostings)
