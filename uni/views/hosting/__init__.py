from flask import Blueprint, render_template, request, g, redirect, url_for
from flask import flash
from flask.ext.login import login_user

from uni import hosting, invoice, user
from uni.views.user import SignupForm

bp = Blueprint('hosting', __name__, url_prefix='/hosting')
hosting_api = hosting.API()
invoice_api = invoice.API()
user_api = user.API()


@bp.route('/')
def index():
    products = hosting_api.get_products()
    apptypes = hosting_api.get_app_types()
    return render_template('hosting/index.html',
                           apptypes=apptypes,
                           products=products)


@bp.route('/trial', methods=["POST", "GET"])
def trial():
    if g.user:
        if not hosting_api.trial_validate(g.user.username):
            return render_template('hosting/trial.html', used=1)

        if request.method == "POST" and "apply" in request.form:
            user = user_api.get_by_username(g.user.username)
            invoice = invoice_api.create(user['id'], None)
            invoice_api.mark_as_paid(invoice['id'])
            product = hosting_api.get_trial_product()
            hosting = hosting_api.create(user['id'], product['id'])
            invoice_api.create_item(invoice['id'], hosting['id'],
                                    product['id'], 'trial',
                                    00.00, 00.00, '15d', 'hosting')
            hosting_api.active(hosting['id'], '15d')
            return redirect(url_for('user.hosting.index'))

    form = SignupForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        user = user_api.get_by_username(username)
        if user:
            flash("Username already exists", "error")
        else:
            user = user_api.create(username, password)
            login_user(user_api.load_user(user['id']))
            return redirect(url_for("hosting.trial"))

    return render_template('hosting/trial.html', form=form)
