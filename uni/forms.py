from flask.ext.babel import lazy_gettext as __
from wtforms import TextField, PasswordField, BooleanField, validators
from flask.ext.wtf.form import Form


class SignupForm(Form):
    username = TextField(
        __(u'Username (E-mail):'),
        [validators.Required(), validators.Email()],
        description='Please enter your email address as the username'
    )
    password = PasswordField(
        __(u'Password:'),
        [
            validators.Required(),
            validators.Length(
                min=6,
                message=__('At least %(num)s characters long', num=6)
            )
        ],
        description=(
            "Please enter your password, "
            "it should be more than 6 characters"
        )
    )
    confirm = PasswordField(
        __('Confirm Password:'),
        [
            validators.EqualTo('password', message='Passwords must match'),
        ],
        description="Please enter your password again"
    )
    accept_tos = BooleanField(
        __('I accept the TOS'),
        [validators.Required(message=__("(Please accept the TOS)"))]
    )


class LoginForm(Form):
    username = TextField(
        __(u'Username (E-mail):'),
        [validators.Required(), validators.Email()]
    )
    password = PasswordField(__(u'Password:'), [validators.Required()])
    remember_me = BooleanField('Remember me')


class ContactForm(Form):
    name = TextField(
        __('Name:'),
        [validators.Required()]
    )
    contact_email = TextField(
        'Email:',
        [validators.Required()]
    )
    company = TextField(
        __('Company:')
    )
    telephone = TextField(
        __('Telephone:'),
        [validators.Required()]
    )
    address = TextField(
        __('Address:'),
        [validators.Required()]
    )
    postcode = TextField(
        __('Postcode:'),
        [validators.Required()]
    )


class PasswordChangeForm(Form):
    password = PasswordField(
        __(u'Password:'),
        [
            validators.Required(),
            validators.Length(
                min=6,
                message=__('At least %(num)s characters long', num=6)
            )
        ],
        description=(
            "Please enter your password, "
            "it should be more than 6 characters"
        )
    )
    confirm = PasswordField(
        __('Confirm Password:'),
        [
            validators.EqualTo('password', message='Passwords must match'),
        ],
        description="Please enter your password again"
    )


class HostingWebsiteForm(Form):
    domain = TextField(__(u'Domain:'),
                       [
                           validators.Required(),
                       ],
                       description="Please enter your domain. "
                       "You don't need to put www at the beginning. "
                       "For example, example.com")
