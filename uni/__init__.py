import logging

from flask import Flask
from flask.ext.babel import Babel
from flask.ext.cache import Cache
from flask.ext.debugtoolbar import DebugToolbarExtension
from flask.ext.mail import Mail
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.babel import lazy_gettext as __
from uni.session import RedisSessionInterface
from uni.log import file_handler

app = Flask(__name__)
cache = Cache()
mail = Mail()
db = SQLAlchemy()
login_manager = LoginManager()


def create_app(env="production"):
    if env == "development":
        app.config.from_object("uni.config.DevelopmentConfig")
    elif env == "production":
        app.config.from_object("uni.config.ProductionConfig")
    else:
        app.config.from_object("uni.config.Config")

    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.INFO)
    app.session_interface = RedisSessionInterface()
    cache.init_app(app)
    mail.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    Babel(app)
    DebugToolbarExtension(app)

    login_manager.login_view = "user.login"
    login_manager.login_message = __("Please log in to access this page.")

    from uni.views import hosting, user, domain, admin
    from uni.views.user import hosting as user_hosting, domain as user_domain

    app.register_blueprint(user.bp)
    app.register_blueprint(hosting.bp)
    app.register_blueprint(domain.bp)
    app.register_blueprint(admin.bp)
    app.register_blueprint(user_hosting.bp)
    app.register_blueprint(user_domain.bp)

    return app
